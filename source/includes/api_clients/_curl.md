## cURL

### Generic cURL Request

The example on the right hand side shows how
to interact with the Norad API using cURL.

```shell
curl -X <HTTP_VERB> -H "Authorization:Token token=<my_norad_api_token>" \
     -H "Accept: application/json" \
     -d '{"key1":"value1", "key2":"value2"}'
     https://norad.cisco.com:8443/v1/<end-point>
```

### Examples using cURL:

### Get a list of Machines in an Organization:

```shell
curl -X GET -H "Authorization:Token token=<my_norad_api_token>" \
     -H "Accept: application/json" \
     https://norad.cisco.com:8443/v1/organizations/{organization.slug}/machines
```

### Get a list of Organizations:

```shell
curl -X GET -H "Authorization:Token token=<my_norad_api_token>" \
     -H "Accept: application/json" \
     https://norad.cisco.com:8443/v1/organizations
```

### Start a Scan for an Organization:

```shell
curl -X POST -H "Authorization:Token token=<my_norad_api_token>" \
     -H "Accept: application/json" \
     -H "Content-Type: application/json" \
     https://norad.cisco.com:8443/v1/organizations/{organization.slug}/scan
```

## SSH Key Pair Assignments

These endpoints allow assigning key pairs to machines.

### Create a Key Pair Assignment

```http
POST /v1/machines/5/ssh_key_pair_assignment HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "ssh_key_pair_id": 1
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 17,
    "machine_id": 5,
    "ssh_key_pair_id": 1,
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z"
  }
}
```

Assigns an SSH Key Pair to the specified Machine.

#### HTTP Request

`POST /v1/machines/{machine.id}/ssh_key_pair_assignment`

#### Body Parameters

Parameter | Description
--------- | -----------
ssh_key_pair_id | The id of the key pair to assign

### Delete a Key Pair Assignment

```http
DELETE /v1/machines/5/ssh_key_pair_assignment HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "ssh_key_pair_id": 17
}
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Unassigns the specified key pair from the machine.

#### HTTP Request

`DELETE /v1/machines/{machine.id}/ssh_key_pair_assignment`

#### Body Parameters

Parameter | Description
--------- | -----------
ssh_key_pair_id | The id of the key pair to unassign

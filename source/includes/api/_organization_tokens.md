## Organization Tokens

Organization tokens are a shared secret used to verify a machine's or relay's enrollment in to an organization.  Each organization is given one token.  Once a machine or relay has been enrolled in to an organization the machine/relay no longer needs to use the organization token.  Organization tokens do not authenticate a user and do not give any user the ability to start scans, read results, or change configurations.  Organization tokens may be recreated by any admin user in the organization in the event that an admin user leaves the organization or the token is unintentionally disclosed.  Once an Organization token is recreated any previous values for that token will be instantly deactivated.

### Recreate an Organization Token

```http
PUT /v1/organizations/demo-org/organization_token HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "value": <new-organization-token-value>,
    "state": "active"
  }
}
```

Recreates the Organization Token for a given Organization.

#### HTTP Request

`PUT /v1/organizations/{organization.slug}/organization_token`

#### PUT Parameters

None

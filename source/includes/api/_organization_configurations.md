## Organization Configurations

### Retreive a specific Configuration

```http
GET /v1/organization_configurations/4 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "auto_approve_docker_relays": false,
    "use_relay_ssh_key": false,
    "enforce_requirements": false,
    "organization_id": 4
  }
}
```

Retrieves the details of the requested organization configuration.

#### HTTP Request

`GET /v1/organization_configurations/{organization_configuration.id}`

#### GET Parameters

None

### Update a Configuration

```http
PUT /v1/organization_configurations/4 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "organization_configurations": {
    "auto_approve_docker_relays": 1
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "auto_approve_docker_relays": true,
    "use_relay_ssh_key": false,
    "enforce_requirements": false,
    "organization_id": 4
  }
}
```

Update settings configuration settings.

#### HTTP Request

`PUT /v1/organization_configurations/{organization_configuration.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
organization[use_relay_ssh_key] | If true, authenticated scans will use SSH keys stored on the Relay host. Otherwise, SSH keys stored in the Norad Cloud will be used instead.
organization[auto_approve_docker_relays] | Toggle whether or not Relays added to the organization should be auto-approved
organization[enforce_requirements] | Toggle whether or not the organization should enforce requirements

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

## Repository Admins

### List Repository Admins for a Repository

Returns an index listing of all repository admins for the
specified repository. Requires the user to have admin privileges for the
associated repository.

```http
GET /v1/security_test_repositories/43/repository_members HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "user": {
        "id": 2,
        "email": "blhitchc@cisco.com",
        "uid": "blhitchc",
        "firstname": "Blake",
        "lastname": "Hitchcock",
        "created_at": "2016-03-24T00:55:59.388Z",
        "updated_at": "2016-03-24T00:55:59.388Z",
        "api_token": "{my_api_token}"
      },
      "security_test_repository_id": 43,
      "created_at": "2016-10-28T15:12:19.978Z",
      "updated_at": "2016-11-03T17:50:26.828Z"
    },
    {
      "id": 2,
      "user": {
        "id": 5,
        "email": "jaboukhe@cisco.com",
        "uid": "jaboukhe",
        "firstname": "Jamil",
        "lastname": "Boukheir",
        "created_at": "2016-03-24T00:55:59.388Z",
        "updated_at": "2016-03-24T00:55:59.388Z",
        "api_token": "{my_api_token}"
      },
      "security_test_repository_id": 43,
      "created_at": "2016-10-28T15:12:19.978Z",
      "updated_at": "2016-11-03T17:50:26.828Z"
    }
  ],
  "count": 2
}
```

#### HTTP Request

`GET /v1/security_test_repositories/{security_test_repository.id}/repository_members HTTP/1.1`

#### GET Parameters

None

### Create a Repository Admin

```http
POST /v1/security_test_repositories/43/repository_members HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "repository_member": {
    "uid": "blhitchc"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "user": {
      "id": 2,
      "email": "blhitchc@cisco.com",
      "uid": "blhitchc",
      "firstname": "Blake",
      "lastname": "Hitchcock",
      "created_at": "2016-03-24T00:55:59.388Z",
      "updated_at": "2016-03-24T00:55:59.388Z",
      "api_token": "{my_api_token}"
    },
    "security_test_repository_id": 43,
    "created_at": "2016-10-28T15:12:19.978Z",
    "updated_at": "2016-11-03T17:50:26.828Z"
  }
}
```

Add a user as an admin for the repository. Requires the requesting user to have
admin privileges on the repository.

#### HTTP Request

`POST
/v1/security_test_repositories/{security_test_repository.id}/repository_members`

#### Body Parameters

Parameter | Description
--------- | -----------
repository_member[uid] | The user id to make admin for this repository

### Remove a Repository Admin

```http
DELETE /v1/repository_members/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes a repository admin. Requires the requesting user to have admin
privileges on the repository.

#### HTTP Request

`DELETE /v1/repository_members/{repository_member.id}`

#### Body Parameters

None

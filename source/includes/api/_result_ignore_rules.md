## Result Ignore Rules

### List Ignore Rules for an Organization

Returns an index listing of all ignore rules for an organization.

```http
GET /v1/organizations/esnden-org/result_ignore_rules HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "comment": "a comment",
      "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
      "ignore_scope_id": 1,
      "ignore_scope_type": "Organization",
      "created_by": "esnden",
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z"
    }
  ],
  "count": 1
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/result_ignore_rules HTTP/1.1`

#### GET Parameters

None

### Create an Ignore Rule for an Organization

```http
POST /v1/organizations/esnden-org/result_ignore_rules HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "result_ignore_rule": {
    "comment": "a comment",
    "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
    "created_by": "esnden"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "comment": "a comment",
    "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
    "ignore_scope_id": 1,
    "ignore_scope_type": "Organization",
    "created_by": "esnden",
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z"
  }
}
```

Creates a new result ignore rule for the organization. This rule will be
applied to all scan results within the organization.

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/result_ignore_rules`

#### Body Parameters

Parameter | Description
--------- | -----------
result_ignore_rule[comment] | An optional comment to add to this rule
result_ignore_rule[signature] | The result signature this rule should apply to
result_ignore_rule[created_by] | The user id of the user who created this rule

### List Ignore Rules for a Machine

Returns an index listing of all ignore rules for a machine.

```http
GET /v1/machines/1/result_ignore_rules HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "comment": "a comment",
      "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
      "ignore_scope_id": 1,
      "ignore_scope_type": "Machine",
      "created_by": "esnden",
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z"
    }
  ],
  "count": 1
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/result_ignore_rules HTTP/1.1`

#### GET Parameters

None

### Create an Ignore Rule for a Machine

```http
POST /v1/machines/1/result_ignore_rules HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "result_ignore_rule": {
    "comment": "a comment",
    "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
    "created_by": "esnden"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "comment": "a comment",
    "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
    "ignore_scope_id": 1,
    "ignore_scope_type": "Machine",
    "created_by": "esnden",
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z"
  }
}
```

Creates a new result ignore rule for the machine. This rule will be
applied to all scan results for this machine.

#### HTTP Request

`POST /v1/machines/{machine.id}/result_ignore_rules`

#### Body Parameters

Parameter | Description
--------- | -----------
result_ignore_rule[comment] | An optional comment to add to this rule
result_ignore_rule[signature] | The result signature this rule should apply to
result_ignore_rule[created_by] | The user id of the user who created this rule


### Retreive a specific Ignore Rule

```http
GET /v1/result_ignore_rules/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "comment": "a comment",
    "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
    "ignore_scope_id": 1,
    "ignore_scope_type": "Machine",
    "created_by": "esnden",
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z"
  }
}
```

Retrieves the details of the requested ignore rule.

#### HTTP Request

`GET /v1/result_ignore_rules/{result_ignore_rule.id}`

#### GET Parameters

None

### Update an Ignore Rule

```http
PUT /v1/result_ignore_rules/23 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "result_ignore_rule": {
    "comment": "a comment"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "comment": "a comment",
    "signature": "aec070645fe53ee3b3763059376134f058cc337247c978add178b6ccdfb0019f",
    "ignore_scope_id": 1,
    "ignore_scope_type": "Machine",
    "created_by": "esnden",
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z"
  }
}
```

Updates an existing ignore rule.

#### HTTP Request

`PUT /v1/result_ignore_rules/{result_ignore_rule.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
result_ignore_rule[comment] | An optional comment to add to this rule

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete an Ignore Rule

```http
DELETE /v1/result_ignore_rules/23 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing ignore rule.

#### HTTP Request

`DELETE /v1/result_ignore_rules/{result_ignore_rule.id}`

#### Body Parameters

None


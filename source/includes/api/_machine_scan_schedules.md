## Machine Scan Schedules

Scans can be scheduled on either a daily or weekly basis. If it is scheduled to
run weekly, a day of the week must be provided. In both cases, a time to run
the scan is required, in 24-hour format. So, for a weekly scan, a value such as
"Mon 12:32" is expected. For a daily scan, a value such as "22:15" is expected.
All times are UTC.

Creating a scan schedule for a machine will result in the machine being scanned according to the
specified schedule.

### List all Schedules

Returns an index listing of all scan schedules for a machine.

```http
GET /v1/machines/asig/scan_schedules HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 2,
  "response": [
    {
      "id": 4,
      "period": "daily",
      "at": "12:12",
      "machine_id": 4,
      "created_at": "2016-03-24T00:55:59.388Z",
      "updated_at": "2016-03-24T00:55:59.388Z"
    },
    {
      "id": 5,
      "period": "weekly",
      "at": "Mon 24:24",
      "machine_id": 4,
      "created_at": "2016-03-24T00:55:59.388Z",
      "updated_at": "2016-03-24T00:55:59.388Z"
    },
  ]
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/scan_schedules HTTP/1.1`

#### GET Parameters

`None`

### Create a Schedule

Creates a new scan schedule for the associated machine.
Note: only one can be created per machine.

```http
POST /v1/machines/asig/scan_schedules HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "period": "daily",
  "at": "12:12"
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "period": "daily",
    "at": "12:12",
    "machine_id": 4,
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z"
  }
}
```

#### HTTP Request

`POST /v1/machines/{machine.id}/scan_schedules`

#### Body Parameters

Parameter | Description
--------- | -----------
machine_scan_schedule[period] | Set the scan to run either weekly or daily
machine_scan_schedule[at] | Set the time (in 24-format) and day (for weekly scans) for the scan
to run

### Retrieve a Specific Schedule

```http
GET /v1/machine_scan_schedules/4 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "period": "daily",
    "at": "12:12",
    "machine_id": 4,
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z"
  }
}
```

Retrieves the details of the requested machine scan schedule.

#### HTTP Request

`GET /v1/machine_scan_schedules/{machine_scan_schedule.id}`

#### GET Parameters

None

### Update a Schedule

```http
PUT /v1/machine_scan_schedules/4 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "machine_scan_schedules": {
    "period": "weekly",
    "at": "Mon 12:12"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "auto_approve_docker_relays": true,
    "use_relay_ssh_key": false,
    "enforce_requirements": false,
    "machine_id": 4
  }
}
{
  "response": {
    "id": 4,
    "period": "weekly",
    "at": "Mon 12:12",
    "machine_id": 4,
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z"
  }
}
```

Update schedule.

#### HTTP Request

`PUT /v1/machine_scan_schedules/{machine_scan_schedule.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
machine_scan_schedule[period] | Set the scan to run either weekly or daily
machine_scan_schedule[at] | Set the time (in 24-format) and day (for weekly scans) for the scan
to run

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Schedule

Removes an existing schedule for the machine.

```http
DELETE /v1/machine_scan_schedules/4 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
```

#### HTTP Request

`DELETE /v1/machine_scan_schedules/{machine_scan_schedule.id}`

#### Body Parameters

`None`

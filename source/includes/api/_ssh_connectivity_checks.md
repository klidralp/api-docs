## SSH Connectivity Checks

### List SSH Connectivity Checks for a Machine

Returns a list of all the ssh connectivity checks that have been created or
updated in the last 24 hours for the specified machine. Requires the user to
have the `organization_admin` or `organization_reader` role in the organization
corresponding to the specified machine. A check has finished running when the `finished_at` field is populated. If the check does not complete within 10 minutes, its `status` will be set to `failing` if no newer checks have been started since.

```http
GET /v1/machines/5/ssh_connectivity_checks HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 3,
  "response": [
    {
      "id": 9,
      "finished_at": "2016-04-21T00:55:59.388Z",
      "created_at": "2016-04-21T00:55:59.388Z",
      "updated_at": "2016-04-21T00:55:59.388Z",
      "machine_id": 5,
      "type": "SshConnectivityCheck",
      "status": "passing",
      "status_reason": null
    },
    {
      "id": 1,
      "finished_at": null,
      "created_at": "2016-04-21T00:55:59.388Z",
      "updated_at": "2016-04-21T00:55:59.388Z",
      "machine_id": 5,
      "type": "SshConnectivityCheck",
      "status": null,
      "status_reason": null
    },
    {
      "id": 10,
      "finished_at": "2016-04-21T00:55:59.388Z",
      "created_at": "2016-04-21T00:55:59.388Z",
      "updated_at": "2016-04-21T00:55:59.388Z",
      "machine_id": 5,
      "type": "SshConnectivityCheck",
      "status": "failing",
      "status_reason": "The connectivity check did not succeed within 10 minutes."
      }
    }
  ]
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/ssh_connectivity_checks HTTP/1.1`

#### GET Parameters

`None`

### Retrieve a specific SSH Connectivity Check for a Machine

Returns the SSH Connectivity Check specified by `id`. Requesting a check with an
id of `latest` will return the last updated check for the specified machine.

```http
GET /v1/machines/5/ssh_connectivity_checks/latest HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 9,
    "finished_at": "2016-04-21T00:55:59.388Z",
    "created_at": "2016-04-21T00:55:59.388Z",
    "updated_at": "2016-04-21T00:55:59.388Z",
    "machine_id": 5,
    "type": "SshConnectivityCheck",
    "status": "passing",
    "status_reason": null
  }
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/ssh_connectivity_checks/{connectivity_check.id} HTTP/1.1`

#### GET Parameters

`None`

### Start an SSH Connectivity Check for a Machine

```http
POST /v1/machines/5/ssh_connectivity_checks HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "finished_at": null,
    "created_at": "2016-04-21T00:55:59.388Z",
    "updated_at": "2016-04-21T00:55:59.388Z",
    "type": "SshConnectivityCheck",
    "machine_id": 5,
    "status": null,
    "status_reason": null
  }
}
```

Starts a new SSH Connectivity Check for the specified machine. The machine must
have either an SSH key assigned to it or its organization must have the "Use
Relay SSH Key" configuration option enabled. The check will timeout after 10
minutes and be purged from Norad after 24 hours.

Note that if a machine has multiple SSH services defined, Norad must be able to
successfully SSH to all of them for the check to succeed. In other words, if a
machine has an SSH service on port 22 and another on port 2222, both must be
passing for the check to pass.

#### HTTP Request

`POST /v1/machines/{machine.id}/ssh_connectivity_checks`

#### Body Parameters

None

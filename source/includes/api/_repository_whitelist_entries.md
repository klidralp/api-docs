## Repository Whitelist Entries

### List Repository Whitelist Entries for a Repository

Returns an index listing of all repository whitelist entries for the
specified repository. These represent repositories that have been whitelisted
for the organizations. Requires the user to have admin privileges for the
associated repository.

```http
GET /v1/security_test_repositories/43/repository_whitelist_entries HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "organization": {
        "id": 23,
        "slug": "norad-org-default",
        "uid": "norad-org-default"
      },
      "security_test_repository_id": 43,
      "created_at": "2016-10-28T15:12:19.978Z",
      "updated_at": "2016-11-03T17:50:26.828Z"
    },
    {
      "id": 2,
      "organization": {
        "id": 33,
        "slug": "user-org-default",
        "uid": "user-org-default"
      },
      "security_test_repository_id": 43,
      "created_at": "2016-10-28T15:12:19.978Z",
      "updated_at": "2016-11-03T17:50:26.828Z"
    }
  ],
  "count": 2
}
```

#### HTTP Request

`GET /v1/security_test_repositories/{security_test_repository.id}/repository_whitelist_entries HTTP/1.1`

#### GET Parameters

None

### Create a Repository Whitelist Entry

```http
POST /v1/security_test_repositories/43/repository_whitelist_entries HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "repository_whitelist_entry": {
    "organization_id": 23
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "organization": {
      "id": 23,
      "slug": "norad-org-default",
      "uid": "norad-org-default"
    },
    "security_test_repository_id": 43,
    "created_at": "2016-10-28T15:12:19.978Z",
    "updated_at": "2016-11-03T17:50:26.828Z"
  }
}
```

Add a repository to the specified organization's whitelist. Requires the user
to have admin privileges on both the repository and organization.

#### HTTP Request

`POST /v1/security_test_repositories/{security_test_repository.id}/repository_whitelist_entries`

#### Body Parameters

Parameter | Description
--------- | -----------
repository_whitelist_entry[organization_id] | The id of the organization for which to whitelist this repository.

### Delete a Repository Whitelist Entry

```http
DELETE /v1/repository_whitelist_entry/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes a repository from the organization whitelist, specified by its whitelist entry `id`. Only the repository admin can perform this action.

#### HTTP Request

`DELETE /v1/repository_whitelist_entry/{repository_whitelist_entry.id}`

#### Body Parameters

None

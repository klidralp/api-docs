## Ping Connectivity Checks

### List Ping Connectivity Checks for a Machine

Returns a list of all the ping connectivity checks that have been created or
updated in the last 24 hours for the specified machine. Requires the user to
have the `organization_admin` or `organization_reader` role in the organization
corresponding to the specified machine. A check has finished running when the
`finished_at` field is populated. If the check does not complete within 10
minutes, its `status` will be set to `failing` if no newer checks have been
started since.

```http
GET /v1/machines/5/ping_connectivity_checks HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 3,
  "response": [
    {
      "id": 9,
      "finished_at": "2016-04-21T00:55:59.388Z",
      "created_at": "2016-04-21T00:55:59.388Z",
      "updated_at": "2016-04-21T00:55:59.388Z",
      "machine_id": 5,
      "type": "PingConnectivityCheck",
      "status": "passing",
      "status_reason": null
    },
    {
      "id": 1,
      "finished_at": null,
      "created_at": "2016-04-21T00:55:59.388Z",
      "updated_at": "2016-04-21T00:55:59.388Z",
      "machine_id": 5,
      "type": "PingConnectivityCheck",
      "status": null,
      "status_reason": null
    },
    {
      "id": 10,
      "finished_at": "2016-04-21T00:55:59.388Z",
      "created_at": "2016-04-21T00:55:59.388Z",
      "updated_at": "2016-04-21T00:55:59.388Z",
      "machine_id": 5,
      "type": "PingConnectivityCheck",
      "status": "failing",
      "status_reason": "The connectivity check did not succeed within 10 minutes."
      }
    }
  ]
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/ping_connectivity_checks HTTP/1.1`

#### GET Parameters

`None`

### Retrieve a specific Ping Connectivity Check for a Machine

Returns the Ping Connectivity Check specified by `id`. Requesting a check with an
id of `latest` will return the last updated check for the specified machine.

```http
GET /v1/machines/5/ping_connectivity_checks/latest HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 9,
    "finished_at": "2016-04-21T00:55:59.388Z",
    "created_at": "2016-04-21T00:55:59.388Z",
    "updated_at": "2016-04-21T00:55:59.388Z",
    "machine_id": 5,
    "type": "PingConnectivityCheck",
    "status": "passing",
    "status_reason": null
  }
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/ping_connectivity_checks/{connectivity_check.id} HTTP/1.1`

#### GET Parameters

`None`

### Start a Ping Connectivity Check for a Machine

```http
POST /v1/machines/5/ping_connectivity_checks HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "finished_at": null,
    "created_at": "2016-04-21T00:55:59.388Z",
    "updated_at": "2016-04-21T00:55:59.388Z",
    "type": "PingConnectivityCheck",
    "status": null,
    "status_reason": null,
    "machine_id": 5
  }
}
```

Starts a new Ping Connectivity Check for the specified machine. The ping
connectivity check performs an Nmap host discovery scan with the following
flags: -sn, -PE, -PM, -PP, -PO, -PR, and if the machine has any services
defined, the appropriate -PS, -PA, and -PU flags are used.

If you're sure your machine is up but is failing the ping check, try explicitly
adding a known reachable service to the machine as this will cause the ping
check to try to reach that specific port and cause the check to pass.


#### HTTP Request

`POST /v1/machines/{machine.id}/ping_connectivity_checks`

#### Body Parameters

None

## Users

### Lookup a User

```http
GET /v1/users/blhitchc HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 2,
    "email": "blhitchc@cisco.com",
    "uid": "blhitchc",
    "firstname": "Blake",
    "lastname": "Hitchcock",
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z",
    "api_token": "{my_api_token}"
  }
}
```

Retrieves the details of the requested user.

#### HTTP Request

`GET /v1/users/{user.id}`

#### GET Parameters

None

<aside class="notice">This endpoint can only be used for looking up the user associated with the provided API token</aside>

### Update a User

```http
PUT /v1/users/blhitchc HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "user": {
    "firstname": "Blayke"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 2,
    "email": "blhitchc@cisco.com",
    "uid": "blhitchc",
    "firstname": "Blayke",
    "lastname": "Hitchcock",
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z",
    "api_token": "{my_api_token}"
  }
}
```

Updates an existing user.

#### HTTP Request

`PUT /v1/users/{user.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
user[firstname] | First name of user
user[lastname] | Last name of user

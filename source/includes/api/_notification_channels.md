## Notification Channels

Notification channels are used to configure what types of events generate notifications and where
those notifications will be sent. Currently, a scan complete email to the members of the
corresponding Organization is the only channel available.

### List all Notification Channels

Returns an index listing of all notification channels for an Organization.

```http
GET /v1/organizations/1/notification_channels HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 4,
      "enabled": true,
      "event": "scan_complete",
      "description": "Scan complete email",
      "organization_id": 4
    }
  ]
}
```

#### HTTP Request

`GET /v1/organizations/{organization.id}/notification_channels`

#### GET Parameters

None

### Retreive a specific Notification Channel

```http
GET /v1/notification_channels/4 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "enabled": true,
    "event": "scan_complete",
    "description": "Scan complete email",
    "organization_id": 4
  }
}
```

Retrieves the details of the requested notification channel.

#### HTTP Request

`GET /v1/notification_channels/{notification_channel.id}`

#### GET Parameters

None

### Update a Notification Channel

```http
PUT /v1/notification_channels/4 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "notification_channel": {
    "enabled": false
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json


{
  "response": {
    "id": 4,
    "enabled": false,
    "event": "scan_complete",
    "description": "Scan complete email",
    "organization_id": 4
  }
}
```

Enable/Disable a notification channel. Requires administrator privileges for the Organization.

#### HTTP Request

`PUT /v1/notification_channels/{notification_channel.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
notification_channel[enabled] | If true, notifications will be sent to the corresponding channel.

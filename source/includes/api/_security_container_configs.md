## Security Container Configs

### List Configs for an Organization

Returns an index listing of all configs for an organization.

```http
GET /v1/organizations/asig/security_container_configs HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "values": {"port": "443"},
      "configurable_id": 1,
      "configurable_type": "Organization",
      "security_container_id": 1,
      "args": ["port"],
      "enabled_outside_of_requirement": false
    }
  ],
  "count": 1
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/security_container_configs HTTP/1.1`

#### GET Parameters

None

### Create a Config for an Organization

```http
POST /v1/organizations/asig/security_container_configs HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_container_config": {
    "values": {"port": 443},
    "security_container_id": 1
    "enabled_outside_of_requirement": true
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "values": {"port": "443"},
    "configurable_id": 1,
    "configurable_type": "Organization",
    "security_container_id": 1,
    "args": ["port"],
    "enabled_outside_of_requirement": true
  }
}
```

Creates a new configuration for the container. This configuration will be used by all machines in
the organization unless they specify their own configuration.

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/security_container_configs`

#### Body Parameters

Parameter | Description
--------- | -----------
security_container_config[values] | Values for arguments in the form of a JSON hash
security_container_config[security_container_id] | ID of Security Container this config applies to
security_container_config[enabled_outside_of_requirement] | Toggles whether or not this container should be turned on outside of a requirement

### List Configs for a Machine

Returns an index listing of all configs for a machine.

```http
GET /v1/machines/1/security_container_configs HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "values": {"port": "443"},
      "configurable_id": 1,
      "configurable_type": "Machine",
      "security_container_id": 1,
      "args": ["port"],
      "enabled_outside_of_requirement": false
    }
  ],
  "count": 1
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/security_container_configs HTTP/1.1`

#### GET Parameters

None

### Create a Config for a Machine

```http
POST /v1/machines/1/security_container_configs HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_container_config": {
    "values": {"port": 443},
    "security_container_id": 1
    "enabled_outside_of_requirement": false
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "values": {"port": "443"},
    "configurable_id": 1,
    "configurable_type": "Machine",
    "security_container_id": 1,
    "args": ["port"],
    "enabled_outside_of_requirement": false
  }
}
```

Creates a new configuration for the container. This configuration will be used by this machine only.

#### HTTP Request

`POST /v1/machines/{machine.id}/security_container_configs`

#### Body Parameters

Parameter | Description
--------- | -----------
security_container_config[values] | Values for arguments in the form of a JSON hash
security_container_config[security_container_id] | ID of Security Container this config applies to
security_container_config[enabled_outside_of_requirement] | Toggles whether or not this container should be turned on outside of a requirement

### Retreive a specific Config

```http
GET /v1/security_container_configs/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "values": {"port": "443"},
    "configurable_id": 1,
    "configurable_type": "Organization",
    "security_container_id": 1,
    "args": ["port"],
    "enabled_outside_of_requirement": false
  }
}
```

Retrieves the details of the requested config.

#### HTTP Request

`GET /v1/security_container_configs/{security_container_config.id}`

#### GET Parameters

None

### Update a Config

```http
PUT /v1/security_container_configs/23 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_container_config": {
    "values": {"port": 8443}
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "values": {"port": "8443"},
    "configurable_id": 1,
    "configurable_type": "Organization",
    "security_container_id": 1,
    "args": ["port"],
    "enabled_outside_of_requirement": false
  }
}
```

Updates an existing config.

#### HTTP Request

`PUT /v1/security_container_configs/{security_container_config.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
security_container_config[values] | Values for arguments in the form of a JSON hash
security_container_config[enabled_outside_of_requirement] | Toggles whether or not this container should be turned on outside of a requirement

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Config

```http
DELETE /v1/security_container_configs/23 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing config.

#### HTTP Request

`DELETE /v1/security_container_configs/{security_container_config.id}`

#### Body Parameters

None

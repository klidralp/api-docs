## Services

Services are representations of the programs running on the machine you are testing that you are
testing. At a high level, a Service is a port number, such as 443, that Norad can use alongside the
IP or FQDN information present with the associated Machine. Services also provide a few additional
attributes to allow you to tell Norad more about the Service that you'd like to test. The
information you provide for the Service you are creating will be used by Norad when determining what
tests are appropriate to run against the Machine. Currently, there are three types of Services: SSH,
Web Application, and Generic.

If your service requires authentication credentials, you should add a [Service Identity](#service-identities).

If your web service requires a specific starting page or you would like to blacklist any URLs from test runs,
or if you would like it to have a custom authentication form, you should add a [Web Application Config](#web-application-configs).

### List all Services for a Machine

Returns an index listing of all services for a machine.

```http
GET /v1/machines/1/services HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "name": "MyString",
      "description": "MyText",
      "port": 1,
      "port_type": "tcp",
      "encryption_type": "ssl",
      "machine_id": 1,
      "type": "WebApplicationService",
      "allow_brute_force": false,
      "discovered": false,
      "created_at": "2018-05-09T20:58:32.193Z",
      "updated_at": "2018-05-09T20:58:32.193Z",
      "application_type": null,
      "service_identity": null,
      "web_application_config": null
    },
    {
      "id": 2,
      "name": "My Sample Web Service",
      "description": "The Web service running on my server",
      "port": 443,
      "port_type": "tcp",
      "encryption_type": "ssl",
      "machine_id": 1,
      "type": "WebApplicationService",
      "allow_brute_force": false,
      "discovered": false,
      "created_at": "2018-05-10T16:07:52.570Z",
      "updated_at": "2018-05-10T16:07:52.570Z",
      "application_type": {
        "name": "https",
        "port": 443,
        "transport_protocol": "tcp"
      },
      "service_identity": null,
      "web_application_config": null
    },
    {
      "id": 3,
      "name": "My Postgres Service",
      "description": "The Postgres service running on my server",
      "port": 5432,
      "port_type": "tcp",
      "encryption_type": "ssl",
      "machine_id": 1,
      "type": "GenericService",
      "allow_brute_force": true,
      "discovered": false,
      "created_at": "2018-05-10T16:10:17.553Z",
      "updated_at": "2018-05-10T16:10:17.553Z",
      "application_type": {
        "name": "postgresql",
        "port": 5432,
        "transport_protocol": "tcp"
      },
      "service_identity": null
    }
  ]
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/services HTTP/1.1`

#### GET Parameters

None

### Create an SSH Service

If your machine has an SSH server listening and you would like to perform authenticated scans (scans
that run as a user on the machine using SSH credentials) against the machine, you can add an SSH
service to the machine.

```http
POST /v1/machines/1/services HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "service": {
    "name": "My Sample SSH Service",
    "description": "The SSH service running on my server",
    "port": 22,
    "port_type": "tcp",
    "encryption_type": "ssh",
    "type": "SshService",
    "allow_brute_force": false
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "name": "My Sample SSH Service",
    "description": "The SSH service running on my server",
    "port": 22,
    "port_type": "tcp",
    "encryption_type": "ssh",
    "machine_id": 1,
    "type": "SshService",
    "allow_brute_force": false,
    "discovered": false,
    "created_at": "2018-05-10T16:15:54.568Z",
    "updated_at": "2018-05-10T16:15:54.568Z",
    "application_type": {
      "name": "ssh",
      "port": 22,
      "transport_protocol": "tcp"
    },
    "service_identity": null
  }
}
```

#### HTTP Request

`POST /v1/machines/{machine.id}/services`

#### Body Parameters

Parameter | Description
--------- | -----------
service[name] | The name to give the service
service[description] | An optional description of the service
service[port] | The port number the service listens on
service[port_type] | The type of port (must be one of **tcp** or **udp**)
service[encryption_type] | The type of encryption used by the service (must be one of **ssl**, **ssh**, or **cleartext**)
service[type] | The type of service (must be one of **GenericService**, **SshService**, or **WebApplicationService**)
service[allow_brute_force] | Whether to perform brute forcing tests on this service (must be **true** or **false**)

### Create a Web Application Service

If your machine runs a Web Application you would like to test, you can add a Web Application service
to the machine. This is required if you wish to run Norad's web application tests against your
machine.

```http
POST /v1/machines/1/services HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "service": {
    "name": "My Sample Web Service",
    "description": "The Web service running on my server",
    "port": 443,
    "port_type": "tcp",
    "encryption_type": "ssl",
    "type": "WebApplicationService",
    "allow_brute_force": false
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "name": "My Sample Web Service",
    "description": "The Web service running on my server",
    "port": 443,
    "port_type": "tcp",
    "encryption_type": "ssl",
    "machine_id": 1,
    "type": "WebApplicationService",
    "allow_brute_force": false,
    "discovered": false,
    "created_at": "2018-05-10T16:07:52.570Z",
    "updated_at": "2018-05-10T16:07:52.570Z",
    "application_type": {
      "name": "https",
      "port": 443,
      "transport_protocol": "tcp"
    },
    "service_identity": null,
    "web_application_config": null
  }
}
```

#### HTTP Request

`POST /v1/machines/{machine.id}/services`

#### Body Parameters

Parameter | Description
--------- | -----------
service[name] | The name to give the service
service[description] | An optional description of the service
service[port] | The port number the service listens on
service[port_type] | The type of port (must be one of **tcp** or **udp**)
service[encryption_type] | The type of encryption used by the service (must be one of **ssl**, **ssh**, or **cleartext**)
service[type] | The type of service (must be one of **GenericService**, **SshService**, or **WebApplicationService**)
service[allow_brute_force] | Whether to perform brute forcing tests on this service (must be **true** or **false**)

### Create a Generic Service

For any other types of services that your machine is running (e.g. postgres) that you would like to
test, you can add a Generic Service.

```http
POST /v1/machines/1/services HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "service": {
    "name": "My Postgres Service",
    "description": "The Postgres service running on my server",
    "port": 5432,
    "port_type": "tcp",
    "encryption_type": "ssl",
    "type": "GenericService",
    "allow_brute_force": true
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "name": "My Postgres Service",
    "description": "The Postgres service running on my server",
    "port": 5432,
    "port_type": "tcp",
    "encryption_type": "ssl",
    "machine_id": 1,
    "type": "GenericService",
    "allow_brute_force": true,
    "discovered": false,
    "created_at": "2018-05-10T16:10:17.553Z",
    "updated_at": "2018-05-10T16:10:17.553Z",
    "application_type": {
      "name": "postgresql",
      "port": 5432,
      "transport_protocol": "tcp"
    },
    "service_identity": null
  }
}
```

#### HTTP Request

`POST /v1/machines/{machine.id}/services`

#### Body Parameters

Parameter | Description
--------- | -----------
service[name] | The name to give the service
service[description] | An optional description of the service
service[port] | The port number the service listens on
service[port_type] | The type of port (must be one of **tcp** or **udp**)
service[encryption_type] | The type of encryption used by the service (must be one of **ssl**, **ssh**, or **cleartext**)
service[type] | The type of service (must be one of **GenericService**, **SshService**, or **WebApplicationService**)
service[allow_brute_force] | Whether to perform brute forcing tests on this service (must be **true** or **false**)

### Retrieve a specific Service

```http
GET /v1/services/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
      "id": 1,
      "name": "My Sample SSH Service",
      "description": "The SSH service running on my server",
      "port": 22,
      "port_type": "tcp",
      "encryption_type": "ssh",
      "machine_id": 1,
      "type": "SshService",
      "allow_brute_force": false,
      "discovered": false,
      "created_at": "2018-05-10T16:15:54.568Z",
      "updated_at": "2018-05-10T16:15:54.568Z",
      "application_type": {
        "name": "ssh",
        "port": 22,
        "transport_protocol": "tcp"
      },
      "service_identity": null
  }
}
```

Retrieves the details of the requested service.

#### HTTP Request

`GET /v1/services/{services.id}`

#### GET Parameters

None

### Update a Service

```http
PUT /v1/services/1 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "service": {
    "name": "A new name for my service"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "name": "A new name for my service",
    "description": "The SSH service running on my server",
    "port": 22,
    "port_type": "tcp",
    "encryption_type": "ssh",
    "machine_id": 1,
    "type": "SshService",
    "allow_brute_force": false,
    "discovered": false,
    "created_at": "2018-05-10T16:15:54.568Z",
    "updated_at": "2018-05-10T16:18:41.640Z",
    "application_type": {
      "name": "ssh",
      "port": 22,
      "transport_protocol": "tcp"
    },
    "service_identity": null
  }
}
```

Updates an existing service.

#### HTTP Request

`PUT /v1/services/{service.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
service[name] | The name to give the service
service[description] | An optional description of the service
service[port] | The port number the service listens on
service[port_type] | The type of port (must be one of **tcp** or **udp**)
service[encryption_type] | The type of encryption used by the service (must be one of **ssl**, **ssh**, or **cleartext**)
service[type] | The type of service (must be one of **GenericService**, **SshService**, or **WebApplicationService**)
service[allow_brute_force] | Whether to perform brute forcing tests on this service (must be **true** or **false**)

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Machine

```http
DELETE /v1/services/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing service.

#### HTTP Request

`DELETE /v1/services/{service.id}`

#### Body Parameters

None

### Start Service Discovery

Begins the service discovery process on target machine.

```http
POST /v1/machines/{machine.id}/service_discoveries HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com:8443
Authorization: Token token=my_user_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 555,
    "machine_id": 13696,
    "state": "pending",
    "error_message": null,
    "updated_at": "2017-04-26T21:01:58.370Z",
    "created_at": "2017-04-26T21:01:58.370Z"
  }
}
```

#### HTTP Request

`POST /v1/machines/{machine.id}/service_discoveries`

#### Body Parameters

None

### Retrieve Service Discovery Status

You can retrieve a service discovery status using either the machine id from the machine on which it was run or using the id of the service discovery.
Using the machine id GET method will return the status of all service discoveries run against a particular machine.

```http
GET /v1/machines/{machine.id}/service_discoveries HTTP/1.1
Host: norad.cisco.com:8443
Authorization: Token token=my_user_api_token
```

```http
GET /v1/service_discoveries/{service_discovery.id} HTTP/1.1
Host: norad.cisco.com:8443
Authorization: Token token=my_user_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 555,
    "machine_id": 13696,
    "state": "completed",
    "error_message": "",
    "updated_at": "2017-04-26T21:01:58.370Z",
    "created_at": "2017-04-26T21:01:58.370Z"
  }
}
```

#### HTTP Request
For the status of all service discoveries run against a particular machine:
`GET /v1/machines/{machine.id}/service_discoveries`

For the status of a particular service discovery:
`GET /v1/service_discoveries/{service_discovery.id}`

#### Body Parameters

None

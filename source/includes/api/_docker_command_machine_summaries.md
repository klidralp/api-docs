## Docker Command Machine Summaries

### List Machine Summary for a Docker Command

Returns a statistical summary grouped by machine for the specified docker
command.

```http
GET /v1/docker_commands/5/machine_summary HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 2,
  "response": {
    "machines": [
      {
        "id": 9,
        "latest_assessment_created_at": "2016-03-24T00:55:59.388Z",
        "name": "Machine Name 1",
        "target_address": "machine-1.acme.corp",
        "assessments_count": 2,
        "assessments_with_results_count": 1,
        "assessment_statuses": {
          "pending_scheduling": 2,
          "in_progress": 0,
          "complete": 0
        },
        "results_summary": {
          "passing": 1,
          "erroring": 0,
          "failing": 0,
          "warning": 0,
          "informing": 0
        }
      },
      {
        "id": 11,
        "latest_assessment_created_at": "2016-04-21T00:55:59.388Z",
        "name": "Machine Name 2",
        "target_address": "machine-2.acme.corp",
        "assessments_count": 5,
        "assessments_with_results_count": 0,
        "assessment_statuses": {
          "pending_scheduling": 0,
          "in_progress": 0,
          "complete": 5
        },
        "results_summary": {
          "passing": 0,
          "erroring": 0,
          "failing": 5,
          "warning": 0,
          "informing": 0
        }
      }
    ]
  }
}
```

#### HTTP Request

`GET /v1/docker_commands/{docker_command.id}/machine_summary HTTP/1.1`

#### GET Parameters

`None`

## Assessments

### List all Assessments for a docker_command

Returns an index listing of all assessments for a given docker_command. The
docker_command may be either a machine-based scan or an organization scan.

```http
GET /v1/docker_commands/1/assessments HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "identifier": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "state": "complete",
      "category": "evaluative",
      "title": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "description": null,
      "machine_id": 1,
      "machine_name": "Test Machine",
      "docker_command_id": 16,
      "security_container_id": 5,
      "type": "WhiteBoxAssessment",
      "created_at": "2016-03-04T17:19:27.934Z",
      "updated_at": "2016-03-04T17:19:27.934Z",
      "results": [
        {
          "id": 4,
          "output": "it has a secure SSH config",
          "status": "pass",
          "title": "SSH Config",
          "description": "Check SSH configuration",
          "ignored": false
        }
      ]
    },
    {
      "id": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "identifier": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "state": "complete",
      "category": "evaluative",
      "title": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "description": null,
      "machine_id": 5,
      "machine_name": "Web Server",
      "docker_command_id": 16,
      "security_container_id": 5,
      "type": "WhiteBoxAssessment",
      "created_at": "2016-03-04T17:19:27.934Z",
      "updated_at": "2016-03-04T17:19:27.934Z",
      "associated_requirements": [
        {
          "id": 14,
          "name": "SEC-AUT-ACCDEF",
          "description": null,
          "requirement_group_id": 1,
          "created_at": "2016-08-16T15:00:33.743Z",
          "updated_at": "2016-08-16T15:00:33.743Z"
        }
      ],
      "results": [
        {
          "id": 4,
          "output": "it has a secure SSH config",
          "status": "pass",
          "title": "SSH Config",
          "description": "Check SSH configuration",
          "ignored": false
        }
      ]
    }
  ]
}
```

#### HTTP Request

`GET /v1/docker_commands/{docker_command.id}/assessments HTTP/1.1`

#### GET Parameters

None

### List all Assessments for a machine

Returns an index listing of all assessments for a machine.

```http
GET /v1/machines/1/assessments HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "identifier": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "state": "complete",
      "category": "evaluative",
      "title": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "description": null,
      "machine_id": 1,
      "docker_command_id": 16,
      "security_container_id": 5,
      "type": "WhiteBoxAssessment",
      "created_at": "2016-03-04T17:19:27.934Z",
      "updated_at": "2016-03-04T17:19:27.934Z",
      "results": [
        {
          "id": 4,
          "output": "it has a secure SSH config",
          "status": "pass",
          "title": "SSH Config",
          "description": "Check SSH configuration",
          "ignored": false
        }
      ]
    },
    {
      "id": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "identifier": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "state": "complete",
      "category": "evaluative",
      "title": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "description": null,
      "machine_id": 1,
      "docker_command_id": 16,
      "security_container_id": 5,
      "type": "WhiteBoxAssessment",
      "created_at": "2016-03-04T17:19:27.934Z",
      "updated_at": "2016-03-04T17:19:27.934Z",
      "associated_requirements": [
        {
          "id": 14,
          "name": "SEC-AUT-ACCDEF",
          "description": null,
          "requirement_group_id": 1,
          "created_at": "2016-08-16T15:00:33.743Z",
          "updated_at": "2016-08-16T15:00:33.743Z"
        }
      ],
      "results": [
        {
          "id": 4,
          "output": "it has a secure SSH config",
          "status": "pass",
          "title": "SSH Config",
          "description": "Check SSH configuration",
          "ignored": false
        }
      ]
    }
  ]
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/assessments HTTP/1.1`

#### GET Parameters

Parameter | Description | Required |
--------- | ----------- | -------- |
docker_command_id | Limit assessments to a given Docker Command ID | false

### List latest Assessments

Returns an index listing the most recent white box assessments for a machine.

```http
GET /v1/machines/1/assessments/latest HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "identifier": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "state": "complete",
      "category": "evaluative",
      "title": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "description": null,
      "machine_id": 1,
      "docker_command_id": 16,
      "security_container_id": 5,
      "type": "WhiteBoxAssessment",
      "associated_requirements": [
        {
          "id": 14,
          "name": "SEC-AUT-ACCDEF",
          "description": null,
          "requirement_group_id": 1,
          "created_at": "2016-08-16T15:00:33.743Z",
          "updated_at": "2016-08-16T15:00:33.743Z"
        }
      ],
      "created_at": "2016-03-04T17:19:27.934Z",
      "updated_at": "2016-03-04T17:19:27.934Z",
      "results": [
        {
          "id": 4,
          "output": "it has a secure SSH config",
          "status": "pass",
          "title": "SSH Config",
          "description": "Check SSH configuration",
          "ignored": false
        }
      ]
    },
    {
      "id": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "identifier": "82e55d48475257a4ec66ea89161311d4af8eea90",
      "state": "complete",
      "category": "evaluative",
      "title": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "description": null,
      "machine_id": 1,
      "docker_command_id": 16,
      "security_container_id": 5,
      "type": "WhiteBoxAssessment",
      "created_at": "2016-03-04T17:19:27.934Z",
      "updated_at": "2016-03-04T17:19:27.934Z",
      "results": [
        {
          "id": 4,
          "output": "it has a secure SSH config",
          "status": "pass",
          "title": "SSH Config",
          "description": "Check SSH configuration",
          "ignored": false
        }
      ]
    }
  ]
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/assessments/latest HTTP/1.1`

#### GET Parameters

None

### Retrieve a specific Assessment

```http
GET /v1/assessments/82e55d48475257a4ec66ea89161311d4af8eea90 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": "82e55d48475257a4ec66ea89161311d4af8eea90",
    "identifier": "82e55d48475257a4ec66ea89161311d4af8eea90",
    "state": "complete",
    "category": "evaluative",
    "title": "norad-registry.cisco.com:5000/serverspec:0.0.1",
    "description": null,
    "machine_id": 1,
    "docker_command_id": 16,
    "security_container_id": 5,
    "type": "WhiteBoxAssessment",
    "associated_requirements": [
      {
        "id": 14,
        "name": "SEC-AUT-ACCDEF",
        "description": null,
        "requirement_group_id": 1,
        "created_at": "2016-08-16T15:00:33.743Z",
        "updated_at": "2016-08-16T15:00:33.743Z"
      }
    ],
    "created_at": "2016-03-04T17:19:27.934Z",
    "updated_at": "2016-03-04T17:19:27.934Z",
    "results": [
      {
        "id": 4,
        "output": "it has a secure SSH config",
        "status": "pass",
        "title": "SSH Config",
        "description": "Check SSH configuration",
        "ignored": false
      }
    ]
  }
}
```

Retrieves the details and results of the requested white box assessment.

#### HTTP Request

`GET /v1/assessments/{assessment.identifier}`

#### GET Parameters

None

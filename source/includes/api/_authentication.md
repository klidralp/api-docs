## Authentication

Authentication is performed via an API token in the ```Authorization``` HTTP header. To
each request, simply include your API token in the request.

> Sample authenticated request:

```http
GET /v1/organizations HTTP/1.1
Authorization: Token token=my_norad_api_token
```

> Make sure to replace `my_norad_api_token` with your API key.

> To retrieve your API token, you must access your User Info page in the [Norad Web UI](https://wwwin-norad.cisco.com).

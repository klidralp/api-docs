## Enforcements

An Enforcement is an object that ties an Organization and a Requirement Group together. In other
words, it enforces the Requirements in the Requirement Group on the Organization.

### List all Enforcements

Returns an index listing of all enforcements for an organization.

```http
GET /v1/organizations/asig/enforcements HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "requirement_group_id": 1,
      "organization_id": 4
    }
  ],
  "count": 1
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/enforcements HTTP/1.1`

#### GET Parameters

None

### Create an Enforcement

```http
POST /v1/organizations/asig/enforcements HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "enforcement": {
    "requirement_group_id": 1
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    {
      "id": 1,
      "requirement_group_id": 1,
      "organization_id": 4
    }
  }
}
```

Creates a new machine in the specified organization.

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/enforcements`

#### Body Parameters

Parameter | Description
--------- | -----------
enforcement[requirement_group_id] | The ID of the Requirement Group to enforce

### Delete an Enforcement

```http
DELETE /v1/enforcements/10 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing enforcement.

#### HTTP Request

`DELETE /v1/enforcements/{enforcement.id}`

#### Body Parameters

None

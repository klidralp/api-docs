## Security Containers

### List all public Containers

Returns an index listing of all Security Containers in the system belonging to
the Norad official repository.

```http
GET /v1/security_containers HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "name": "nmap-ssl-dh-param:0.0.1",
      "prog_args": "-p %{port} %{target}",
      "default_config": {
        "port": 443
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 443,
        "name": "https",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 2,
      "name": "serverspec:0.0.1",
      "prog_args": "-h %{target} -u %{ssh_user} -t %{test_regex} -p %{port} %{ssh_key}",
      "default_config": {
        "port": 22,
        "test_regex": "**/*_spec.rb"
      },
      "category": "whitebox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 22,
        "name": "ssh",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 3,
      "name": "sslyze-heartbleed:0.0.1",
      "prog_args": "%{target}:%{port}",
      "default_config": {
        "port": 443
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 443,
        "name": "https",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 4,
      "name": "qualys:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {
        "username": "",
        "password": "",
        "option_title": "DAVA Unauthenticated",
        "scanner": "RTP-2"
      },
      "category": "blackbox",
      "configurable": true,
      "help_url": null,
      "application_type": null,
      "test_types": [
        "authenticated",
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
      "security_test_repository_id": 4
    }
  ],
  "count": 4
}
```

#### HTTP Request

`GET /v1/security_containers`

#### GET Parameters

Parameter | Description
--------- | -----------
test_name | Optionally filter by test name

### List all Containers available for an organization

Returns an index listing of all Security Containers in the system belonging to
the Norad official repository and in the specified organization's whitelist.

```http
GET /v1/organizations/noraduser-org-default/security_containers HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 144,
      "name": "custom-security-test:0.0.1",
      "prog_args": "-p %{port} %{target}",
      "default_config": {
        "port": 443
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 443,
        "name": "https",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 2,
        "name": "Acme Corp Official Repository",
        "public": false,
        "official": false
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "org-repo-1.acmecorp.com:5000/custom-security-test:0.0.1",
      "security_test_repository_id": 2
    },
    {
      "id": 1,
      "name": "nmap-ssl-dh-param:0.0.1",
      "prog_args": "-p %{port} %{target}",
      "default_config": {
        "port": 443
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 443,
        "name": "https",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 2,
      "name": "serverspec:0.0.1",
      "prog_args": "-h %{target} -u %{ssh_user} -t %{test_regex} -p %{port} %{ssh_key}",
      "default_config": {
        "port": 22,
        "test_regex": "**/*_spec.rb"
      },
      "category": "whitebox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 22,
        "name": "ssh",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 3,
      "name": "sslyze-heartbleed:0.0.1",
      "prog_args": "%{target}:%{port}",
      "default_config": {
        "port": 443
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 443,
        "name": "https",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 4,
      "name": "qualys:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {
        "username": "",
        "password": "",
        "option_title": "DAVA Unauthenticated",
        "scanner": "RTP-2"
      },
      "category": "blackbox",
      "configurable": true,
      "help_url": null,
      "application_type": null,
      "test_types": [
        "authenticated",
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
      "security_test_repository_id": 4
    }
  ],
  "count": 5
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/security_containers`

#### GET Parameters

Parameter | Description
--------- | -----------
test_name | Optionally filter by test name

### List all Containers for a repository

Returns an index listing of all Security Containers in the system belonging to
the specified security test repository. If the user is an admin of the
repository, the repository is the Norad official one, or the repository is in
one of the user's organizations' whitelisted repositories, all attributes for
the containers are returned. Otherwise, only the `name` attribute is returned.

```http
GET /v1/security_test_repositories/4/security_containers HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "name": "nmap-ssl-dh-param:0.0.1",
      "prog_args": "-p %{port} %{target}",
      "default_config": {
        "port": 443
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 443,
        "name": "https",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 2,
      "name": "serverspec:0.0.1",
      "prog_args": "-h %{target} -u %{ssh_user} -t %{test_regex} -p %{port} %{ssh_key}",
      "default_config": {
        "port": 22,
        "test_regex": "**/*_spec.rb"
      },
      "category": "whitebox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 22,
        "name": "ssh",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/serverspec:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 3,
      "name": "sslyze-heartbleed:0.0.1",
      "prog_args": "%{target}:%{port}",
      "default_config": {
        "port": 443
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": {
        "port": 443,
        "name": "https",
        "transport_protocol": "tcp"
      },
      "test_types": [
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 4,
      "name": "qualys:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {
        "username": "",
        "password": "",
        "option_title": "DAVA Unauthenticated",
        "scanner": "RTP-2"
      },
      "category": "blackbox",
      "configurable": true,
      "help_url": null,
      "application_type": null,
      "test_types": [
        "authenticated",
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
      "security_test_repository_id": 4
    }
  ],
  "count": 4
}
```

#### HTTP Request

`GET /v1/security_test_repositories/{security_test_repository.id}/security_containers`

#### GET Parameters

Parameter | Description
--------- | -----------
test_name | Optionally filter by test name

### Retreive a specific Container

If the specified container belongs to the Norad official repository, a repository the
user is an admin for, or included in one of the user's organizations'
whitelisted repositories, all attributes are returned. Otherwise, only the
`name` attribute is returned.

```http
GET /v1/security_containers/4 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    {
      "id": 4,
      "name": "qualys:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {
        "username": "",
        "password": "",
        "option_title": "DAVA Unauthenticated",
        "scanner": "RTP-2"
      },
      "category": "blackbox",
      "configurable": true,
      "help_url": null,
      "application_type": null,
      "test_types": [
        "authenticated",
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
      "security_test_repository_id": 4
    }
  }
}
```

Retrieves the details of the requested container.

#### HTTP Request

`GET /v1/security_containers/{security_container.id}`

#### GET Parameters

None

### Create a Security Container

Creates a security container in the specified repository. The user must be an
admin of the repository.

```http
POST /v1/security_test_repositories/4/security_containers HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_container": {
    "name": "qualys:0.0.1",
    "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
    "default_config": {
      "username": "",
      "password": "",
      "option_title": "DAVA Unauthenticated",
      "scanner": "RTP-2"
    },
    "multihost": true,
    "test_types": ["whole_host"],
    "configurable": false,
    "category": "blackbox"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "name": "qualys:0.0.1",
    "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
    "default_config": {
      "username": "",
      "password": "",
      "option_title": "DAVA Unauthenticated",
      "scanner": "RTP-2"
    },
    "category": "blackbox",
    "configurable": true,
    "help_url": null,
    "application_type": null,
    "test_types": [
      "authenticated",
      "whole_host"
    ],
    "security_test_repository": {
      "id": 4,
      "host": "norad-registry.cisco.com:5000",
      "name": "Norad Official Repository",
      "public": true,
      "official": true,
      "username": null,
      "authorizer": {
        "can_read?": true,
        "can_peruse?": true,
        "can_edit?": false
      },
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z"
    },
    "multihost": false,
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z",
    "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
    "security_test_repository_id": 4
  }
}
```

#### HTTP Request

`POST /v1/security_test_repositories/{security_test_repository.id}/security_containers`

#### Body Parameters

Parameter | Description
--------- | -----------
security_container[name] | The docker image name and tag for this container.
security_container[prog_args] | Arguments to pass to the command being run.
security_container[default_config] | The default configuration to use for this container.
security_container[category] | The category for this container. One of `blackbox`, `whitebox`.
security_container[multihost] | Whether or not this container is used for multiple hosts.
security_container[test_types] | Array of test types for this container.
security_container[configurable] | Whether the security container is configurable.
security_container[service] | Optional hash containing attributes of the service this container applies to. Valid sub-params are `name`, `port`, and `transport_protocol`. Uses the nmap database at https://svn.nmap.org/nmap/nmap-services.

### Create Multiple Security Containers

Creates multiple security containers in the specified repository. The user must be an
admin of the repository. The create is transactional, so if one container fails
to save then no containers will be created.

```http
POST /v1/security_test_repositories/4/security_containers HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_containers": [
    {
      "name": "qualys:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {
        "username": "",
        "password": "",
        "option_title": "DAVA Unauthenticated",
        "scanner": "RTP-2"
      },
      "multihost": true,
      "test_types": ["whole_host"],
      "configurable": false,
      "category": "blackbox"
    },
    {
      "name": "http-scanner:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {},
      "multihost": true,
      "test_types": ["whole_host"],
      "configurable": true,
      "category": "blackbox"
    }
  ]
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "name": "qualys:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {
        "username": "",
        "password": "",
        "option_title": "DAVA Unauthenticated",
        "scanner": "RTP-2"
      },
      "category": "blackbox",
      "configurable": false,
      "help_url": null,
      "application_type": null,
      "test_types": [
        "authenticated",
        "whole_host"
      ],
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "multihost": false,
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
      "security_test_repository_id": 4
    },
    {
      "id": 24,
      "name": "http-scanner:0.0.1",
      "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
      "default_config": {},
      "multihost": true,
      "test_types": ["whole_host"],
      "configurable": true,
      "application_type": {
        "name": "http",
        "port": 80,
        "transport_protocol": "tcp"
      },
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "full_path": "norad-registry.cisco.com:5000/http-scanner:0.0.1",
      "security_test_repository": {
        "id": 4,
        "host": "norad-registry.cisco.com:5000",
        "name": "Norad Official Repository",
        "public": true,
        "official": true,
        "username": null,
        "authorizer": {
          "can_read?": true,
          "can_peruse?": true,
          "can_edit?": false
        },
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z"
      },
      "category": "blackbox",
      "security_test_repository_id": 4
    }
  ],
  "count": 2
}
```

#### HTTP Request

`POST /v1/security_test_repositories/{security_test_repository.id}/security_containers`

#### Body Parameters

Parameter | Description
--------- | -----------
security_containers[name] | The docker image name and tag for this container.
security_containers[prog_args] | Arguments to pass to the command being run.
security_containers[default_config] | The default configuration to use for this container.
security_containers[category] | The category for this container. One of `blackbox`, `whitebox`.
security_containers[multihost] | Whether or not this container is used for multiple hosts.
security_containers[test_types] | Array of test types for this container.
security_containers[configurable] | Whether the security container is configurable.
security_containers[service] | Optional hash containing attributes of the service this container applies to. Valid sub-params are `name`, `port`, and `transport_protocol`. Uses the nmap database at https://svn.nmap.org/nmap/nmap-services.

### Update a Security Container

Updates a security container in the specified repository. The user must be an
admin of the repository.

```http
PUT /v1/security_test_repositories/4/security_containers/23 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_container": {
    "name": "qualys:0.0.1",
    "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
    "default_config": {
      "username": "",
      "password": "",
      "option_title": "DAVA Unauthenticated",
      "scanner": "RTP-2"
    },
    "category": "blackbox"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "name": "qualys:0.0.1",
    "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
    "default_config": {
      "username": "",
      "password": "",
      "option_title": "DAVA Unauthenticated",
      "scanner": "RTP-2"
    },
    "category": "blackbox",
    "configurable": false,
    "help_url": null,
    "application_type": null,
    "test_types": [
      "authenticated",
      "whole_host"
    ],
    "security_test_repository": {
      "id": 4,
      "host": "norad-registry.cisco.com:5000",
      "name": "Norad Official Repository",
      "public": true,
      "official": true,
      "username": null,
      "authorizer": {
        "can_read?": true,
        "can_peruse?": true,
        "can_edit?": false
      },
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z"
    },
    "multihost": false,
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z",
    "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
    "security_test_repository_id": 4
  }
}
```

Updates a container.

#### HTTP Request

`PUT /v1/security_test_repositories/{security_test_repository.id}/security_containers/{security_container.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
security_container[name] | The docker image name and tag for this container.
security_container[prog_args] | Arguments to pass to the command being run.
security_container[default_config] | The default configuration to use for this container.
security_container[category] | The category for this container. One of `blackbox`, `whitebox`.
security_container[multihost] | Whether or not this container is used for multiple hosts.
security_container[test_types] | Array of test types for this container.
security_container[configurable] | Whether the security container is configurable.
security_container[service] | Optional hash containing attributes of the service this container applies to. Valid sub-params are `name`, `port`, and `transport_protocol`. Uses the nmap database at https://svn.nmap.org/nmap/nmap-services.

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Container

```http
DELETE /v1/security_containers/23 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing container. The user must be an admin of the repository.

#### HTTP Request

`DELETE /v1/security_containers/{security_container.id}`

#### Body Parameters

None

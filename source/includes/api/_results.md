## Results

### Create a Result

Create a result from within a security container

```http
POST /v1/assessments/da39a3ee5e6b4b0d3255bfef95601890afd80709/results HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
NORAD_SIGNATURE: e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855

{
  timestamp: "1459042797",
  "results": [
    {
      "nid": "qid123191",
      "sir": "medium"
      "status": "fail",
      "title": "XSS Check",
      "output": "<script>alert(1)</script>",
      "description": "An XSS was found in the scanned web application"
    }
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": null
}
```

#### HTTP Request

`POST /v1/assessments/{assessment.id}/results HTTP/1.1`

#### Body Parameters

Parameter | Description
--------- | -----------
results[nid] | Result ID assigned by the container writer (e.g. Qualys qid). Can only contain alphanumeric, dashes, and underscores
results[sir] | Severity for the result.  Must be one of: `unevaluated`, `no_impact`, `low`, `medium`, `high`, `critical`
results[title] | Title of the result
results[status] | Must be one of: `pass`, `fail`, `warn`, `error`, `info`
results[output] | Raw output of the scan
results[description] | Description of the result

<aside class="notice">This endpoint is only updateable via signed request using the container's secret token</aside>

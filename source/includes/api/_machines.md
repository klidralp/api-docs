## Machines

### List all Machines

Returns an index listing of all machines in an organization.

```http
GET /v1/organizations/asig/machines HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 9,
      "ip": null,
      "fqdn": "norad.cisco.com",
      "use_fqdn_as_target": true,
      "name": "aa515634-659d-4119-8932-64eb7a9e99fd",
      "description": "The Norad server",
      "target_address": "norad.cisco.com",
      "latest_assessment_stats": {
        "passing": 2,
        "warning": 0,
        "failing": 0,
        "erroring": 0,
        "informing": 0
      },
      "organization_id": 1,
      "agent": null,
      "ssh_config": null
    },
    {
      "id": 10,
      "ip": "72.163.4.161",
      "fqdn": null,
      "use_fqdn_as_target": false,
      "name": "custom-machine-name",
      "target_address": "72.163.4.161",
      "description": null,
      "latest_assessment_stats": {
        "passing": 2,
        "warning": 0,
        "failing": 5,
        "erroring": 0,
        "informing": 0
      },
      "organization_id": 1,
      "agent": null,
      "ssh_config": null
    }
  ],
  "count": 2
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/machines HTTP/1.1`

#### GET Parameters

None

### Create a Machine

```http
POST /v1/organizations/asig/machines HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "machine": {
    "ip": "192.168.1.100",
    "fqdn": "myserver.local",
    "use_fqdn_as_target": true,
    "target_address": "myserver.local",
    "description": "This is my best server",
    "name": "my custom machine name"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 10,
    "ip": "192.168.1.100",
    "fqdn": "myserver.local",
    "use_fqdn_as_target": true,
    "target_address": "myserver.local",
    "name": "my custom machine name",
    "description": "This is my best server",
    "latest_assessment_stats": {
      "passing": 0,
      "warning": 0,
      "failing": 0,
      "erroring": 0,
      "informing": 0
    },
    "organization_id": 1,
    "agent": null,
    "ssh_config": null
  }
}
```

Creates a new machine in the specified organization.

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/machines`

#### Body Parameters

Parameter | Description
--------- | -----------
machine[ip] | The IP address of the machine
machine[fqdn] | The FQDN of the machine
machine[use_fqdn_as_target] | If the FQDN of the machine should be used as the target for security scans
machine[description] | The description of the machine
machine[name] | The name of the machine. (auto-generated if left blank) **Note:** max length is 255.

### Retreive a specific Machine

```http
GET /v1/machines/10 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 10,
    "ip": "192.168.1.100",
    "fqdn": "myserver.local",
    "use_fqdn_as_target": true,
    "target_address": "myserver.local",
    "enabled_tests_count": 0,
    "description": "This is my best server",
    "name": "machine name",
    "latest_assessment_stats": {
      "passing": 0,
      "warning": 0,
      "failing": 0,
      "erroring": 0,
      "informing": 0
    },
    "organization_id": 1,
    "agent": null,
    "ssh_config": null
  }
}
```

Retrieves the details of the requested machine.

#### HTTP Request

`GET /v1/machines/{machine.id}`

#### GET Parameters

None

### Update a Machine

```http
PUT /v1/machines/10 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "machine": {
    "ip": "192.1681.100",
    "description": "a new description",
    "fqdn": "myserver.local",
    "use_fqdn_as_target": true,
    "target_address": "myserver.local",
    "name": "aa515634-659d-4119-8932-64eb7a9e99fd"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 10,
    "ip": "192.168.1.100",
    "fqdn": "myserver.local",
    "use_fqdn_as_target": true,
    "target_address": "myserver.local",
    "name": "aa515634-659d-4119-8932-64eb7a9e99fd",
    "description": "a new description",
    "latest_assessment_stats": {
      "passing": 0,
      "warning": 0,
      "failing": 0,
      "erroring": 0,
      "informing": 0
    },
    "organization_id": 1,
    "agent": null,
    "ssh_config": null
  }
}
```

Updates an existing machine.

#### HTTP Request

`PUT /v1/machines/{machine.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
machine[ip] | The IP address of the machine
machine[fqdn] | The FQDN of the machine
machine[use_fqdn_as_target] | If the FQDN of the machine should be used as the target for security scans
machine[description] | The description of the machine
machine[name] | The name of the machine. (auto-generated if left blank) **Note:** max length is 255.

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Machine

```http
DELETE /v1/machines/10 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing machine.

#### HTTP Request

`DELETE /v1/machines/{machine.id}`

#### Body Parameters

None

## Requirement Groups

This endpoint supports creation of Requirement Groups.

### List all Requirement Groups

Returns an index listing of all of the requirement groups. Only groups that the
current user administers are returned if the `with_admin` parameter is set to
`true`.

```http
GET /v1/requirement_groups?with_admin=true HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 3,
      "name": "My Baseline Requirements",
      "description": "Stuff that is important to me",
      "requirements": [
        {
          "id": 25,
          "name": "SEC-NO-SNOWFLAKE",
          "description": "No special snowflakes!",
          "requirement_group_id": 3,
          "created_at": "2016-10-28T15:12:19.978Z",
          "updated_at": "2016-11-03T17:50:26.828Z"
        }
      ]
    }
  ]
}
```

#### HTTP Request

`GET /v1/requirement_groups`

#### GET Parameters

Parameter | Description
--------- | -----------
with_admin | Boolean flag to filter index to requirement groups you own


### Create a Requirement Group

Creates a new requirement group.

```http
POST /v1/requirement_groups HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "requirement_group": {
    "name": "My Baseline Requirements",
    "description": "Stuff that is important to me"
   }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "description": "Stuff that is important to me",
    "id": 26,
    "name": "My Baseline Requirements",
    "requirements": []
  }
}
```

#### HTTP Request

`POST /v1/requirement_groups`

#### Body Parameters

Parameter | Description
--------- | -----------
requirement_group[name]        | The name you would like to set for the requirement group
requirement_group[description] | A brief description you would like to set for the requirement group

### Retrieve a specific Requirement Group

Retrieves the details of the requested requirement group.

```http
GET /v1/requirement_groups/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "name": "PSB",
    "description": "Collection of PSB Requirements",
    "requirements": [
      {
        "id": 1,
        "name": "PSB-1",
        "description": null,
        "requirement_group_id": 1,
        "created_at": "2016-10-28T14:54:42.479Z",
        "updated_at": "2016-10-28T14:54:42.479Z"
      },
      {
        "id": 2,
        "name": "PSB-2",
        "description": null,
        "requirement_group_id": 1,
        "created_at": "2016-10-28T14:54:42.490Z",
        "updated_at": "2016-10-28T14:54:42.490Z"
      }
    ]
  }
}
```

#### HTTP Request

`GET /v1/requirement_groups/{requirement_group.id}`

#### GET Parameters

None

### Update a Requirement Group

Updates an existing requirement group.

```http
PUT /v1/requirement_groups/3 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "requirement_group": {
    "name": "A New Name",
    "description": "A new description"
   }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 3,
    "name": "A New Name",
    "description": "A new description",
    "requirements": [
      {
        "id": 25,
        "name": "SEC-NO-SNOWFLAKE",
        "description": "No special snowflakes!",
        "requirement_group_id": 3,
        "created_at": "2016-10-28T15:12:19.978Z",
        "updated_at": "2016-11-03T17:50:26.828Z"
      }
    ]
  }
}
```

#### HTTP Request

`PUT /v1/requirement_groups/{requirement_group.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
requirement_group[name]        | The name of the requirement group
requirement_group[description] | A brief description of the requirement group

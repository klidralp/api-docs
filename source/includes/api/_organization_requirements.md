## Organization Requirements

This endpoint provides a listing of the requirements that are enforced for a given organization. It
provides a list of the associated security containers for each requirement.

### List all Requirements

Returns an index listing of all requirements enabled for an organization.

```http
GET /v1/organizations/asig/requirements HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 19,
      "name": "PSB-123",
      "description": "An important requirement",
      "requirement_group_id": 1,
      "provisions": [
        {
          "id": 1,
          "security_container_id": 24,
          "requirement_id": 10
          "security_container": {
            "id": 24,
            "name": "norad-registry.cisco.com:5000/psb-x509-cert-validation:0.0.1",
            "prog_args": "%{target}:%{port}",
            "default_config": {
              "port": "443"
            },
            "category": "blackbox"
          }
        }
      ]
    },
    {
      "id": 21,
      "name": "PSB-321",
      "description": "Another important requirement",
      "requirement_group_id": 1,
      "provisions": [
        {
          "id": 2,
          "security_container_id": 24,
          "requirement_id": 11
          "security_container": {
            "id": 24,
            "name": "norad-registry.cisco.com:5000/psb-x509-cert-validation:0.0.1",
            "prog_args": "%{target}:%{port}",
            "default_config": {
              "port": "443"
            },
            "category": "blackbox"
          }
        }
      ]
    }
  ],
  "count": 2
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/requirements HTTP/1.1`

#### GET Parameters

None

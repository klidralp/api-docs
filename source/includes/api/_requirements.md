## Requirements

This endpoint supports creation and deletion of Requirements within a
Requirement Group.

### Create a Requirement 

Creates a new requirement.

```http
POST /v1/requirement_groups/1/requirements HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "requirement": {
    "name": "SEC-DAT-KEYMGMT",
    "description": "Implement processes and procedures for securely managing encryption keys"
   }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "description": "Implement processes and procedures for securely managing encryption keys",
    "id": 26,
    "name": "SEC-DAT-KEYMGMT",
    "requirement_group_id": 1,
    "provisions": []
  }
}
```

#### HTTP Request

`POST /v1/requirement_groups/{requirement_group.id}/requirements`

#### Body Parameters

Parameter | Description
--------- | -----------
requirement[name]        | The name you would like to set for the requirement
requirement[description] | A brief description you would like to set for the requirement

### Retrieve a specific Requirement

Retrieves the details of the requested requirement

```http
GET /v1/requirements/25 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 25,
    "name": "SEC-NO-SNOWFLAKE",
    "description": "No special snowflakes!",
    "requirement_group_id": 3,
    "provisions": [
      {
        "id": 76,
        "security_container_id": 20,
        "requirement_id": 25,
        "security_container": {
          "id": 20,
          "name": "norad-registry.cisco.com:5000/nmap-list-tcp-ports:0.0.1",
          "prog_args": "-sS -p 1-65535 %{target}",
          "default_config": {},
          "category": "blackbox",
          "configurable": false
        }
      }
    ]
  }
}
```

#### HTTP Request

`GET /v1/requirements/{requirement.id}`

#### GET Parameters

None

### Update a Requirement

Updates an existing requirement.

```http
PUT /v1/requirements/25 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "requirement": {
    "name": "New Name"
    "description": "New description"
   }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "description": "New description",
    "id": 26,
    "name": "New Name",
    "requirement_group_id": 3,
    "provisions": []
  }
}
```

#### HTTP Request

`PUT /v1/requirements/{requirement.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
requirement[name]        | The name of the requirement
requirement[description] | A brief description of the requirement

### Delete a Requirement

Removes an existing requirement.

```http
DELETE /v1/requirements/26 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

#### HTTP Request

`DELETE /v1/requirements/{requirement.id}`

#### Body Parameters

None

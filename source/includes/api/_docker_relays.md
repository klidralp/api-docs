## Docker Relays

### List all Relays for an Organization

Returns an index listing of all Relays in an organization.

```http
GET /v1/organizations/asig/docker_relays HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 67,
      "public_key": "",
      "queue_name": "noradqueue-d5c36a983a3e95b0b5a2c335eab926e67149339eb7f8c8e9cfc1fddad4d1e7a0",
      "state": "offline",
      "last_heartbeat": "2016-03-22T01:34:51.414Z",
      "verified": true,
      "organization_id": 1,
      "key_signature": "e4:d1:91:ae:bb:26:4f:c8:ee:52:71:e5:e2:88:bb:d3:b1:3a:bb:12:61:7d:84:11:38:57:ec:f8:0a:43:31:46",
      "public_file_encryption_key": "",
      "last_reported_version": "0.0.6",
      "latest_version": "0.0.8",
      "outdated": true
    }
  ],
  "count": 1
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/docker_relays HTTP/1.1`

#### GET Parameters

None

### Create a Relay

```http
POST /v1/organizations/asig/machines HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com

{
  "organization_token": "token_of_the_org_to_put_the_relay_in",
  "docker_relay": {
    "public_key": "{base64_encoded_public_rsa_key}"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 67,
    "public_key": "{base64_encoded_public_rsa_key}"
    "queue_name": "noradqueue-d5c36a983a3e95b0b5a2c335eab926e67149339eb7f8c8e9cfc1fddad4d1e7a0",
    "state": "offline",
    "last_heartbeat": "2016-03-22T01:34:51.414Z",
    "verified": false,
    "organization_id": 1,
    "key_signature": "e4:d1:91:ae:bb:26:4f:c8:ee:52:71:e5:e2:88:bb:d3:b1:3a:bb:12:61:7d:84:11:38:57:ec:f8:0a:43:31:46",
    "public_file_encryption_key": "{base64_encoded_public_rsa_key_for_encrypting_containers_file}",
    "last_reported_version": null,
    "latest_version": null,
    "outdated": false
  }
}
```

Creates a new, unverified Relay in the specified organization.

<aside class="notice">This endpoint does not require an API token</aside>

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/docker_relays`

#### Body Parameters

Parameter | Description
--------- | -----------
organization_token | The organization token of the organization you wish to add the machine to
docker_relay[public_key] | A Base64 encoded public key corresponding to the Relay's private key

### Retreive a specific Relay

```http
GET /v1/docker_relays/67 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 67,
    "public_key": "{base64_encoded_public_rsa_key}"
    "queue_name": "noradqueue-d5c36a983a3e95b0b5a2c335eab926e67149339eb7f8c8e9cfc1fddad4d1e7a0",
    "state": "offline",
    "last_heartbeat": "2016-03-22T01:34:51.414Z",
    "verified": false,
    "organization_id": 1,
    "key_signature": "e4:d1:91:ae:bb:26:4f:c8:ee:52:71:e5:e2:88:bb:d3:b1:3a:bb:12:61:7d:84:11:38:57:ec:f8:0a:43:31:46",
    "public_file_encryption_key": "{base64_encoded_public_rsa_key_for_encrypting_containers_file}"
    "last_reported_version": "0.0.6",
    "latest_version": "0.0.8",
    "outdated": true
  }
}
```

Retrieves the details of the requested relay.

<aside class="notice">This endpoint can also be accessed with a signed request using the Relay's
private key</aside>

#### HTTP Request

`GET /v1/docker_relays/{docker_relay.id}`

#### GET Parameters

None

### Update a Relay

```http
PUT /v1/docker_relays/67 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "docker_relays": {
    "verified": true
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 67,
    "public_key": "{base64_encoded_public_rsa_key}"
    "queue_name": "noradqueue-d5c36a983a3e95b0b5a2c335eab926e67149339eb7f8c8e9cfc1fddad4d1e7a0",
    "state": "offline",
    "last_heartbeat": "2016-03-22T01:34:51.414Z",
    "verified": true,
    "organization_id": 1,
    "key_signature": "e4:d1:91:ae:bb:26:4f:c8:ee:52:71:e5:e2:88:bb:d3:b1:3a:bb:12:61:7d:84:11:38:57:ec:f8:0a:43:31:46",
    "public_file_encryption_key": "{base64_encoded_public_rsa_key_for_encrypting_containers_file}"
    "last_reported_version": "0.0.6",
    "latest_version": "0.0.8",
    "outdated": true
  }
}
```

Updates an existing Relay.

#### HTTP Request

`PUT /v1/docker_relays/{docker_relay.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
docker_relay[verified] | Toggle the boolean value to verify a Relay for use

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Relay

```http
DELETE /v1/docker_relays/67 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes a Relay.

#### HTTP Request

`DELETE /v1/docker_relays/{docker_relay.id}`

#### Body Parameters

None

<aside class="notice">This endpoint can also be accessed with a signed request using the Relay's
private key</aside>

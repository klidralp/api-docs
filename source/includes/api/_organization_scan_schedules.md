## Organization Scan Schedules

Scans can be scheduled on either a daily or weekly basis. If it is scheduled to
run weekly, a day of the week must be provided. In both cases, a time to run
the scan is required, in 24-hour format. So, for a weekly scan, a value such as
"Mon 12:32" is expected. For a daily scan, a value such as "22:15" is expected.
All times are UTC.

Creating a scan schedule for an organization will result in all machines within
the organization being scanned according to the specified schedule.

### List all Schedules

Returns an index listing of all scan schedules for an organization.

```http
GET /v1/organizations/asig/scan_schedules HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 2,
  "response": [
    {
      "id": 4,
      "period": "daily",
      "at": "12:12",
      "organization_id": 4,
      "created_at": "2016-03-24T00:55:59.388Z",
      "updated_at": "2016-03-24T00:55:59.388Z"
    },
    {
      "id": 5,
      "period": "weekly",
      "at": "Mon 24:24",
      "organization_id": 4,
      "created_at": "2016-03-24T00:55:59.388Z",
      "updated_at": "2016-03-24T00:55:59.388Z"
    },
  ]
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/scan_schedules HTTP/1.1`

#### GET Parameters

`None`

### Create a Schedule

Creates a new scan schedule for the associated organization.
Note: only one can be created per organization.

```http
POST /v1/organizations/asig/scan_schedules HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "period": "daily",
  "at": "12:12"
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "period": "daily",
    "at": "12:12",
    "organization_id": 4,
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z"
  }
}
```

#### HTTP Request

`POST /v1/organizations/{organization.slug}/scan_schedules`

#### Body Parameters

Parameter | Description
--------- | -----------
organization_scan_schedule[period] | Set the scan to run either weekly or daily
organization_scan_schedule[at] | Set the time (in 24-format) and day (for weekly scans) for the scan
to run

### Retrieve a Specific Schedule

```http
GET /v1/organization_scan_schedules/4 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "period": "daily",
    "at": "12:12",
    "organization_id": 4,
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z"
  }
}
```

Retrieves the details of the requested organization scan schedule.

#### HTTP Request

`GET /v1/organization_scan_schedules/{organization_scan_schedule.id}`

#### GET Parameters

None

### Update a Schedule

```http
PUT /v1/organization_scan_schedules/4 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "organization_scan_schedules": {
    "period": "weekly",
    "at": "Mon 12:12"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 4,
    "auto_approve_docker_relays": true,
    "use_relay_ssh_key": false,
    "enforce_requirements": false,
    "organization_id": 4
  }
}
{
  "response": {
    "id": 4,
    "period": "weekly",
    "at": "Mon 12:12",
    "organization_id": 4,
    "created_at": "2016-03-24T00:55:59.388Z",
    "updated_at": "2016-03-24T00:55:59.388Z"
  }
}
```

Update schedule.

#### HTTP Request

`PUT /v1/organization_scan_schedules/{organization_scan_schedule.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
organization_scan_schedule[period] | Set the scan to run either weekly or daily
organization_scan_schedule[at] | Set the time (in 24-format) and day (for weekly scans) for the scan
to run

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Schedule

Removes an existing schedule for the organization.

```http
DELETE /v1/organization_scan_schedules/4 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
```

#### HTTP Request

`DELETE /v1/organization_scan_schedules/{organization_scan_schedule.id}`

#### Body Parameters

`None`

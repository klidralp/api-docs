## Security Test Repositories

### List Repositories

Returns an index listing of all repositories. Only the `name`, `public` and
`id` attributes are returned.

```http
GET /v1/security_test_repositories HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "name": "Norad Test Repository",
      "public": true,
      "official": true
    },
    {
      "id": 24,
      "name": "Company Internal Repository",
      "public": false,
      "official": false
    }
  ],
  "count": 2
}
```

#### HTTP Request

`GET /v1/security_test_repositories HTTP/1.1`

#### GET Parameters

None

### Create a Security Test Repository

```http
POST /v1/security_test_repositories HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_test_repository": {
    "host": "norad-registry.cisco.com:5000",
    "name": "Norad Test Repository",
    "public": false,
    "username": "repository-user",
    "password": "repository-password"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "host": "norad-registry.cisco.com:5000",
    "name": "Norad Test Repository",
    "public": false,
    "official": false,
    "username": "repository-user",
    "authorizer": {
      "can_read?": true,
      "can_peruse?": true,
      "can_edit?": true
    },
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z"
  }
}
```

Creates a new repository. The current user will be set as the repository admin,
and only this user can update or destroy this repository.

#### HTTP Request

`POST /v1/security_test_repositories`

#### Body Parameters

Parameter | Description
--------- | -----------
security_test_repository[host] | Docker registry host. If the port is not included, the Docker default is used instead (5000).
security_test_repository[name] | A name for this repository.
security_test_repository[public] | (default: false) Whether the repository is public or not. If public, this repository's attributes (except `password`) will be accessible by all users.
security_test_repository[username] | (optional) The username to use for authenticating to this repository.
security_test_repository[password] |  (required if username present) The password to use for authenticating to this repository.

### Retreive a specific Repository

```http
GET /v1/security_test_repositories/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "host": "norad-registry.cisco.com:5000",
    "name": "Norad Test Repository",
    "public": false,
    "official": true,
    "username": "repository-user"
  }
}
```

Retrieves the details of the requested repository. If the user requesting the
repository is not the repository's admin or the repository is not public, only
the `name` and `public` attributes are returned.

#### HTTP Request

`GET /v1/security_test_repositories/{security_test_repository.id}`

#### GET Parameters

None

### Update a Repository

```http
PUT /v1/security_test_repositories/23 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "security_test_repository": {
    "host": "norad-registry.cisco.com:5000",
    "name": "Norad Test Repository",
    "public": false,
    "username": "repository-user"
    "password": "new-password"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "host": "norad-registry.cisco.com:5000",
    "name": "Norad Test Repository",
    "public": false,
    "official": true,
    "username": "repository-user"
  }
}
```

Updates an existing repository. Only a repository admin can update the
repository.

#### HTTP Request

`PUT /v1/security_test_repositories/{security_test_repository.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
security_test_repository[host] | Docker registry host. If the port is not included, the Docker default is used instead (5000).
security_test_repository[name] | A name for this repository.
security_test_repository[public] | (default: false) Whether the repository is public or not. If public, this repository's attributes (except `password`) will be accessible by all users.
security_test_repository[username] | (optional) The username to use for authenticating to this repository.
security_test_repository[password] |  (required if username present) The password to use for authenticating to this repository.

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Repository

```http
DELETE /v1/security_test_repositories/23 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing repository. Only the repository admin can remove the
repository.

#### HTTP Request

`DELETE /v1/security_test_repositories/{security_test_repositories.id}`

#### Body Parameters

None

## Organization Scan Summaries

### List scan summaries for an Organization

Returns a statistical summary for each organization scan belonging to the
specified organization, referenced by `docker_command.id`. The returned
`created_at` is the latest `created_at` from the scans' assessments. The
returned `state` is the least-progress-made state of the scans' assessments.

```http
GET /v1/organizations/asig/scan_summary HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 2,
  "response": {
    "assessments_summary": {
      "8": {
        "created_at": "2016-03-24T00:55:59.388Z",
        "state": "in_progress",
        "state_details": "Started successfully",
        "machines_scanned": 4,
        "results_summary": {
          "passing": 8,
          "erroring": 4,
          "failing": 2,
          "warning": 0,
          "informing": 0
        }
      },
      "11": {
        "created_at": "2017-04-21T00:55:59.388Z",
        "state": "complete",
        "state_details": "Finished successfully",
        "machines_scanned": 4,
        "machines_scanned": 8,
        "results_summary": {
          "passing": 28,
          "erroring": 0,
          "failing": 0,
          "warning": 1,
          "informing": 0
        }
      }
    }
  }
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/scan_summary HTTP/1.1`

#### GET Parameters

`None`

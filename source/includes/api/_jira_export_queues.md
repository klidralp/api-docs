## Jira Export Queues

### Create a Jira Export Queue

```http
POST /v1/organizations/asig/jira_export_queues HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "jira_export_queue": {
    "auto_sync": true,
    "custom_jira_configuration_attributes": {
      "title": "Test Jira Project",
      "site_url": "https://myjira.invalid",
      "project_key": "TJP",
      "username": "jira_user",
      "password": "jira_password"
    }
  }
}
```

```http
HTTP/1.1 201 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "type": "jira_export_queue",
    "created_by": "norad_tester",
    "created_at": "2017-04-06T23:49:00.000Z",
    "updated_at": "2017-04-06T23:49:00.000Z",
    "organization_id": 1,
    "auto_sync": true,
    "custom_jira_configuration": {
      "id": 1,
      "created_at": "2017-04-06T23:49:00.000Z",
      "updated_at": "2017-04-06T23:49:00.000Z",
      "title": "Test Jira Project",
      "site_url": "https://myjira.invalid",
      "project_key": "TJP",
      "username": "jira_user",
      "result_export_queue_id": 1
    }
  }
}
```

Creates a new Jira export queue in the specified organization. Use this queue type when you want to
export to a Jira instance other than Infosec's. If you'd like to automatically export results when a
scan is complete, simply set the auto_sync option to true.

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/jira_export_queues`

#### Body Parameters

Parameter | Description
--------- | -----------
organization.slug | The organization slug of the organization to which you wish to add the Jira export queue
jira_export_queue[auto_sync] | Boolean to toggle whether or not results should be auto synced to Jira Jira
jira_export_queue[custom_jira_configuration_attributes][title] | Name to identify the target Jira instance in the Norad UI
jira_export_queue[custom_jira_configuration_attributes][site_url] | URL of the Jira instance
jira_export_queue[custom_jira_configuration_attributes][project_key] | The ID of the project in Jira
jira_export_queue[custom_jira_configuration_attributes][username] |  Username for accessing Jira. Will be stored encrypted
jira_export_queue[custom_jira_configuration_attributes][password] | Password for access ing Jira. Will be stored encrypted and cannot be retrieved via the API

### Retrieve a Specific Jira Export Queue

```http
GET /v1/jira_export_queues/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json
{
  "response": {
    "id": 1,
    "type": "jira_export_queue",
    "created_by": "norad_tester",
    "created_at": "2017-04-06T23:49:00.000Z",
    "updated_at": "2017-04-06T23:49:00.000Z",
    "organization_id": 1,
    "auto_sync": true,
    "custom_jira_configuration": {
      "id": 1,
      "created_at": "2017-04-06T23:49:00.000Z",
      "updated_at": "2017-04-06T23:49:00.000Z",
      "title": "Test Jira Project",
      "site_url": "https://myjira.invalid",
      "project_key": "TJP",
      "username": "jira_user",
      "result_export_queue_id": 1
    }
  }
}
```

Retrieves the details of the requested Jira export queue.

#### HTTP Request

`GET /v1/jira_export_queues/{jira_export_queue.id}`

#### GET Parameters

None

### Update a Jira Export Queue

```http
PUT /v1/jira_export_queues/1 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "jira_export_queue": {
    "auto_sync": false,
    "custom_jira_configuration_attributes": {
      "title": "My Jira Project",
      "site_url": "https://myjira.invalid",
      "project_key": "MJP",
      "username": "jira_user",
      "password": "jira_password"
    }
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "type": "jira_export_queue",
    "created_by": "norad_tester",
    "created_at": "2017-04-06T23:49:00.000Z",
    "updated_at": "2017-04-06T23:49:00.000Z",
    "organization_id": 1,
    "auto_sync": false,
    "custom_jira_configuration": {
      "id": 1,
      "created_at": "2017-04-06T23:49:00.000Z",
      "updated_at": "2017-04-06T23:49:00.000Z",
      "title": "My Jira Project",
      "site_url": "https://myjira.invalid",
      "project_key": "MJP",
      "username": "jira_user",
      "result_export_queue_id": 1
    }
  }
}
```

Updates an existing Jira export queue.

#### HTTP Request

`PUT /v1/jira_export_queues/{jira_export_queue.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
jira_export_queue[auto_sync] | Boolean to toggle whether or not results should be auto synced to Jira Jira
jira_export_queue[custom_jira_configuration_attributes][title] | Name to identify the target Jira instance in the Norad UI
jira_export_queue[custom_jira_configuration_attributes][site_url] | URL of the Jira instance
jira_export_queue[custom_jira_configuration_attributes][project_key] | The ID of the project in Jira
jira_export_queue[custom_jira_configuration_attributes][username] |  Username for accessing Jira. Will be stored encrypted
jira_export_queue[custom_jira_configuration_attributes][password] | Password for access ing Jira. Will be stored encrypted and cannot be retrieved via the API

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Jira Export Queue

```http
DELETE /v1/jira_export_queues/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing Jira export queue.

#### HTTP Request

`DELETE /v1/jira_export_queues/{jira_export_queue.id}`

#### Body Parameters

None

## Docker Commands

### List Docker Commands for an Organization

Returns an index listing of all docker commands for an organization.

```http
GET /v1/organizations/asig/docker_commands HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "error_details": null,
      "security_containers": [
        {
          "id": 1,
          "name": "qualys:0.0.1",
          "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
          "default_config": {
            "username": "",
            "password": "",
            "option_title": "DAVA Unauthenticated",
            "scanner": "RTP-2"
          },
          "category": "blackbox",
          "configurable": false,
          "help_url": null,
          "application_type": null,
          "test_types": [
            "authenticated",
            "whole_host"
          ],
          "security_test_repository": {
            "id": 4,
            "host": "norad-registry.cisco.com:5000",
            "name": "Norad Official Repository",
            "public": true,
            "official": true,
            "username": null,
            "authorizer": {
              "can_read?": true,
              "can_peruse?": true,
              "can_edit?": false
            },
            "created_at": "2016-06-10T18:45:35.284Z",
            "updated_at": "2016-06-10T18:45:35.284Z"
          },
          "multihost": false,
          "created_at": "2016-06-10T18:45:35.284Z",
          "updated_at": "2016-06-10T18:45:35.284Z",
          "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "security_test_repository_id": 4
        }
      ],
      "machine_id": null,
      "organization_id": 1,
      "state": "assessments_created",
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "state_details": "Finished successfully",
      "assessments": [
        {
          "id": "4ebf472030e9c4a2eaa4d00dde36a07a299c1dab",
          "identifier": "4ebf472030e9c4a2eaa4d00dde36a07a299c1dab",
          "state": "pending",
          "category": "evaluative",
          "title": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "description": null,
          "machine_id": 53,
          "docker_command_id": 23,
          "security_container_id": 4,
          "type": "BlackBoxAssessment",
          "created_at": "2016-03-27T01:44:34.556Z",
          "results": []
        },
        {
          "id": "5bdf5fceb6e8d0f527df8651aaf0f3d069c76a90",
          "identifier": "5bdf5fceb6e8d0f527df8651aaf0f3d069c76a90",
          "state": "pending",
          "category": "evaluative",
          "title": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "description": null,
          "machine_id": 52,
          "docker_command_id": 23,
          "security_container_id": 4,
          "type": "BlackBoxAssessment",
          "created_at": "2016-03-27T01:44:34.550Z",
          "results": []
        }
      ]
    },
    {
      "id": 22,
      "error_details": null,
      "security_containers": [
        {
          "id": 1,
          "name": "qualys:0.0.1",
          "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
          "default_config": {
            "username": "",
            "password": "",
            "option_title": "DAVA Unauthenticated",
            "scanner": "RTP-2"
          },
          "category": "blackbox",
          "configurable": false,
          "help_url": null,
          "application_type": null,
          "test_types": [
            "authenticated",
            "whole_host"
          ],
          "security_test_repository": {
            "id": 4,
            "host": "norad-registry.cisco.com:5000",
            "name": "Norad Official Repository",
            "public": true,
            "official": true,
            "username": null,
            "authorizer": {
              "can_read?": true,
              "can_peruse?": true,
              "can_edit?": false
            },
            "created_at": "2016-06-10T18:45:35.284Z",
            "updated_at": "2016-06-10T18:45:35.284Z"
          },
          "multihost": false,
          "created_at": "2016-06-10T18:45:35.284Z",
          "updated_at": "2016-06-10T18:45:35.284Z",
          "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "security_test_repository_id": 4
        }
      ],
      "machine_id": null,
      "organization_id": 1,
      "state": "assessments_created",
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "state_details": "Finished successfully",
      "assessments": [
        {
          "id": "251e818b8f5ca1800f58fa68a8316e1dc3fe2f14",
          "identifier": "251e818b8f5ca1800f58fa68a8316e1dc3fe2f14",
          "state": "complete",
          "category": "evaluative",
          "title": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "description": null,
          "machine_id": 51,
          "docker_command_id": 21,
          "type": "BlackBoxAssessment",
          "security_container_id": 4,
          "created_at": "2016-03-25T22:09:21.993Z",
          "results": [
            {
              "id": 1,
              "output": "No vulnerabilities found",
              "status": "pass",
              "title": "Qualys Scan - No vulnerabilities found",
              "description": "No vulnerabilities found"
            }
          ]
        }
      ]
    }
  ],
  "count": 2
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/docker_commands HTTP/1.1`

#### GET Parameters

None

### Create a Docker Command for an Organization

```http
POST /v1/organizations/asig/docker_commands HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "docker_command": {
    "scan_containers_attributes": [
      {
        "security_container_id": 1
      }
    ]
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "error_details": null,
    "security_containers": [
      {
        "id": 1,
        "name": "qualys:0.0.1",
        "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
        "default_config": {
          "username": "",
          "password": "",
          "option_title": "DAVA Unauthenticated",
          "scanner": "RTP-2"
        },
        "category": "blackbox",
        "configurable": false,
        "help_url": null,
        "application_type": null,
        "test_types": [
          "authenticated",
          "whole_host"
        ],
        "security_test_repository": {
          "id": 4,
          "host": "norad-registry.cisco.com:5000",
          "name": "Norad Official Repository",
          "public": true,
          "official": true,
          "username": null,
          "authorizer": {
            "can_read?": true,
            "can_peruse?": true,
            "can_edit?": false
          },
          "created_at": "2016-06-10T18:45:35.284Z",
          "updated_at": "2016-06-10T18:45:35.284Z"
        },
        "multihost": false,
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z",
        "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
        "security_test_repository_id": 4
      }
    ],
    "machine_id": null,
    "organization_id": 1,
    "state": "pending",
    "state_details": null,
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z",
    "assessments": []
  }
}
```

Creates a new docker command for the specified organization. Spawns one or more Docker containers on
the backend to perform the scan. The containers will be started on a Relay if one is configured for
the organization.

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/docker_commands`

#### Body Parameters

Parameter | Description
--------- | -----------
docker_command[scan_containers_attributes] | An array of `{ security_container_id: {security_container.id} }` objects

### List Docker Commands for a Machine

Returns an index listing of all docker commands for a machine.

```http
GET /v1/machines/1/docker_commands HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 23,
      "error_details": null,
      "security_containers": [
        {
          "id": 1,
          "name": "qualys:0.0.1",
          "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
          "default_config": {
            "username": "",
            "password": "",
            "option_title": "DAVA Unauthenticated",
            "scanner": "RTP-2"
          },
          "category": "blackbox",
          "configurable": false,
          "help_url": null,
          "application_type": null,
          "test_types": [
            "authenticated",
            "whole_host"
          ],
          "security_test_repository": {
            "id": 4,
            "host": "norad-registry.cisco.com:5000",
            "name": "Norad Official Repository",
            "public": true,
            "official": true,
            "username": null,
            "authorizer": {
              "can_read?": true,
              "can_peruse?": true,
              "can_edit?": false
            },
            "created_at": "2016-06-10T18:45:35.284Z",
            "updated_at": "2016-06-10T18:45:35.284Z"
          },
          "multihost": false,
          "created_at": "2016-06-10T18:45:35.284Z",
          "updated_at": "2016-06-10T18:45:35.284Z",
          "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "security_test_repository_id": 4
        }
      ],
      "machine_id": 1,
      "organization_id": null,
      "state": "assessments_created",
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "state_details": "Finished successfully",
      "assessments": [
        {
          "id": "4ebf472030e9c4a2eaa4d00dde36a07a299c1dab",
          "identifier": "4ebf472030e9c4a2eaa4d00dde36a07a299c1dab",
          "state": "pending",
          "category": "evaluative",
          "title": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "description": null,
          "machine_id": 10,
          "docker_command_id": 23,
          "type": "BlackBoxAssessment",
          "security_container_id": 4,
          "created_at": "2016-03-27T01:44:34.556Z",
          "results": []
        }
      ]
    },
    {
      "id": 22,
      "error_details": null,
      "security_containers": [
        {
          "id": 1,
          "name": "qualys:0.0.1",
          "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
          "default_config": {
            "username": "",
            "password": "",
            "option_title": "DAVA Unauthenticated",
            "scanner": "RTP-2"
          },
          "category": "blackbox",
          "configurable": false,
          "help_url": null,
          "application_type": null,
          "test_types": [
            "authenticated",
            "whole_host"
          ],
          "security_test_repository": {
            "id": 4,
            "host": "norad-registry.cisco.com:5000",
            "name": "Norad Official Repository",
            "public": true,
            "official": true,
            "username": null,
            "authorizer": {
              "can_read?": true,
              "can_peruse?": true,
              "can_edit?": false
            },
            "created_at": "2016-06-10T18:45:35.284Z",
            "updated_at": "2016-06-10T18:45:35.284Z"
          },
          "multihost": false,
          "created_at": "2016-06-10T18:45:35.284Z",
          "updated_at": "2016-06-10T18:45:35.284Z",
          "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "security_test_repository_id": 4
        }
      ],
      "machine_id": 1,
      "organization_id": null,
      "state": "assessments_created",
      "created_at": "2016-06-10T18:45:35.284Z",
      "updated_at": "2016-06-10T18:45:35.284Z",
      "state_details": "Finished successfully",
      "assessments": [
        {
          "id": "251e818b8f5ca1800f58fa68a8316e1dc3fe2f14",
          "identifier": "251e818b8f5ca1800f58fa68a8316e1dc3fe2f14",
          "state": "complete",
          "category": "evaluative",
          "title": "norad-registry.cisco.com:5000/qualys:0.0.1",
          "description": null,
          "machine_id": 10,
          "docker_command_id": 21,
          "security_container_id": 4,
          "type": "BlackBoxAssessment",
          "created_at": "2016-03-25T22:09:21.993Z",
          "results": [
            {
              "id": 1,
              "output": "No vulnerabilities found",
              "status": "pass",
              "title": "Qualys Scan - No vulnerabilities found",
              "description": "No vulnerabilities found"
            }
          ]
        }
      ]
    }
  ],
  "count": 2
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/docker_commands HTTP/1.1`

#### GET Parameters

None

### Create a Docker Command for a Machine

```http
POST /v1/machines/1/docker_commands HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "docker_command": {
    "scan_containers_attributes": [
      {
        "security_container_id": 1
      }
    ]
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "error_details": null,
    "security_containers": [
      {
        "id": 1,
        "name": "qualys:0.0.1",
        "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
        "default_config": {
          "username": "",
          "password": "",
          "option_title": "DAVA Unauthenticated",
          "scanner": "RTP-2"
        },
        "category": "blackbox",
        "configurable": false,
        "help_url": null,
        "application_type": null,
        "test_types": [
          "authenticated",
          "whole_host"
        ],
        "security_test_repository": {
          "id": 4,
          "host": "norad-registry.cisco.com:5000",
          "name": "Norad Official Repository",
          "public": true,
          "official": true,
          "username": null,
          "authorizer": {
            "can_read?": true,
            "can_peruse?": true,
            "can_edit?": false
          },
          "created_at": "2016-06-10T18:45:35.284Z",
          "updated_at": "2016-06-10T18:45:35.284Z"
        },
        "multihost": false,
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z",
        "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
        "security_test_repository_id": 4
      }
    ],
    "machine_id": 1,
    "organization_id": null,
    "state": "pending",
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z",
    "state_details": null,
    "assessments": []
  }
}
```

Creates a new docker command for the specified machine. Spawns one or more Docker containers on
the backend to perform the scan. The containers will be started on a Relay if one is configured for
the organization.

#### HTTP Request

`POST /v1/machines/{machine.id}/docker_commands`

#### Body Parameters

Parameter | Description
--------- | -----------
docker_command[scan_containers_attributes] | An array of `{ security_container_id: {security_container.id} }` objects

### Retreive a specific Docker Command

```http
GET /v1/docker_commands/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "error_details": null,
    "security_containers": [
      {
        "id": 1,
        "name": "qualys:0.0.1",
        "prog_args": "-h %{target} -u %{username} -p %{password} -r %{option_title} -q %{scanner}",
        "default_config": {
          "username": "",
          "password": "",
          "option_title": "DAVA Unauthenticated",
          "scanner": "RTP-2"
        },
        "category": "blackbox",
        "configurable": false,
        "help_url": null,
        "application_type": null,
        "test_types": [
          "authenticated",
          "whole_host"
        ],
        "security_test_repository": {
          "id": 4,
          "host": "norad-registry.cisco.com:5000",
          "name": "Norad Official Repository",
          "public": true,
          "official": true,
          "username": null,
          "authorizer": {
            "can_read?": true,
            "can_peruse?": true,
            "can_edit?": false
          },
          "created_at": "2016-06-10T18:45:35.284Z",
          "updated_at": "2016-06-10T18:45:35.284Z"
        },
        "multihost": false,
        "created_at": "2016-06-10T18:45:35.284Z",
        "updated_at": "2016-06-10T18:45:35.284Z",
        "full_path": "norad-registry.cisco.com:5000/qualys:0.0.1",
        "security_test_repository_id": 4
      }
    ],
    "machine_id": null,
    "organization_id": 1,
    "state": "assessments_created",
    "created_at": "2016-06-10T18:45:35.284Z",
    "updated_at": "2016-06-10T18:45:35.284Z",
    "state_details": "Finished successfully",
    "assessments": [
      {
        "id": "4ebf472030e9c4a2eaa4d00dde36a07a299c1dab",
        "identifier": "4ebf472030e9c4a2eaa4d00dde36a07a299c1dab",
        "state": "pending",
        "category": "evaluative",
        "title": "norad-registry.cisco.com:5000/qualys:0.0.1",
        "description": null,
        "machine_id": 53,
        "docker_command_id": 23,
        "security_container_id": 4,
        "type": "BlackBoxAssessment",
        "created_at": "2016-03-27T01:44:34.556Z",
        "results": []
      },
      {
        "id": "5bdf5fceb6e8d0f527df8651aaf0f3d069c76a90",
        "identifier": "5bdf5fceb6e8d0f527df8651aaf0f3d069c76a90",
        "state": "pending",
        "category": "evaluative",
        "title": "norad-registry.cisco.com:5000/qualys:0.0.1",
        "description": null,
        "machine_id": 52,
        "docker_command_id": 23,
        "type": "BlackBoxAssessment",
        "security_container_id": 4,
        "created_at": "2016-03-27T01:44:34.550Z",
        "results": []
      }
    ]
  }
}
```

Retrieves the details of the requested docker command.

#### HTTP Request

`GET /v1/docker_commands/{docker_command.id}`

#### GET Parameters

None

### Delete a Docker Command

```http
DELETE /v1/docker_commands/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes a completed docker command. All associated assessments and results will
be removed as well. Only an organization admin may remove docker commands.
If the docker command is still running, an error is returned instead.

#### HTTP Request

`DELETE /v1/docker_commands/{docker_command.id}`

#### Body Parameters

None

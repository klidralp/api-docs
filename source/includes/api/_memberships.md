## Memberships

### List all Memberships

Returns an index listing of all memberships in an organization.

```http
GET /v1/organizations/asig/memberships HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 2,
  "response": [
    {
      "id": 69,
      "organization_id": 66,
      "user_id": 47
    },
    {
      "id": 70,
      "organization_id": 66,
      "user_id": 48
    }
  ]
}
```

#### HTTP Request

`GET /v1/organizations/{organization.slug}/memberships HTTP/1.1`

#### GET Parameters

`None`

### Create a Membership

Creates a new membership instance to the organization indicated.

```http
POST /v1/organizations/asig/memberships HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "uid": "jodoe",
  "admin": true
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "created_at": "2016-04-08T14:02:54.871Z",
    "id": 76,
    "organization_id": 66,
    "updated_at": "2016-04-08T14:02:54.871Z",
    "user_id": 1
  }
}
```

#### HTTP Request

`POST /v1/organizations/{organization.slug}/memberships`

#### Body Parameters

Parameter | Description
--------- | -----------
uid       | The user UID of the user to receive the new membership
admin     | A boolean value indicating whether the user has the organization_admin role in the organizaiton verses the organization_reader role

### Delete a Membership

Removes an existing membership for the associated organization.
Reader users may remove themselves and admins may remove anyone except the last admin in the organization.

```http
DELETE /v1/memberships/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
```

#### HTTP Request

`DELETE /v1/memberships/{membership.id}`

#### Body Parameters

`None`

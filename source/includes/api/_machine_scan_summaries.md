## Machine Scan Summaries

### List scan summaries for a Machine

Returns a statistical summary for each scan that has targeted the specified
machine, either directly or via an organization scan, referenced by
`docker_command.id`. The returned `created_at` is the latest `created_at` from
the scans' assessments. The returned `state` is the least-progress-made state of
the scans' assessments.

```http
GET /v1/machines/52/scan_summary HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "count": 2,
  "response": {
    "assessments_summary": {
      "8": {
        "created_at": "2016-03-24T00:55:59.388Z",
        "state": "in_progress",
        "state_details": "Started successfully"
      },
      "11": {
        "created_at": "2017-04-21T00:55:59.388Z",
        "state": "complete",
        "state_details": "Finished successfully"
      }
    }
  }
}
```

#### HTTP Request

`GET /v1/machines/{machine.id}/scan_summary HTTP/1.1`

#### GET Parameters

`None`

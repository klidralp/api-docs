## Web Application Configs

Web application configs are configuration values specific to web application services,
that they may or may not need.

### Create a Web Application Config

```http
POST /v1/services/1/web_application_configs HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "web_application_config": {
    "starting_page_path": "/",
    "url_blacklist": "/admin,contact,profile/settings",
    "auth_type": "unauthenticated",
    "login_form_username_field_name": "username",
    "login_form_password_field_name": "password"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "auth_type": "unauthenticated",
    "url_blacklist": "/admin,contact,profile/settings",
    "starting_page_path": "/",
    "login_form_username_field_name": "username",
    "login_form_password_field_name": "password",
    "service_id": 1,
    "created_at": "2018-05-09T21:04:33.669Z",
    "updated_at": "2018-05-09T21:04:33.669Z"
  }
}
```

#### HTTP Request

`POST /v1/services/{service.id}/web_application_configs`

#### Body Parameters

Parameter | Description
--------- | -----------
web_application_config[starting_page_path] | (Default is "/") If your service requires a specific starting page
web_application_config[url_blacklist] | (Default is none) If you would like to blacklist any URLs from test runs
web_application_config[auth_type] | (Default is "unauthenticated") An authentication method for your web service. (**unauthenticated** for no authentication, **digest** for HTTP Digest authentication, **form** for HTML form field authentication, or **basic** for HTTP Basic authentication)
web_application_config[login_form_username_field_name] | (Default is "username") Value for the name attribute for the "username" field of your service's HTML authentication form. (if you choose "form" for authentication method)
web_application_config[login_form_password_field_name] | (Default is "password") Value for the name attribute for the "password" field of your service's HTML authentication form. (if you choose "form" for authentication method)

### Retrieve a specific Web Application Config

```http
GET /v1/web_application_configs/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "auth_type": "unauthenticated",
    "url_blacklist": "/admin,contact,profile/settings",
    "starting_page_path": "/root",
    "login_form_username_field_name": "username",
    "login_form_password_field_name": "password",
    "service_id": 1,
    "created_at": "2018-05-09T21:04:33.669Z",
    "updated_at": "2018-05-09T21:35:53.326Z"
  }
}
```

Retrieves the details of the requested web application config.

#### HTTP Request

`GET /v1/web_application_configs/{web_application_config.id}`

#### GET Parameters

None

### Update a Web Application Config

```http
PUT /v1/web_application_configs/1 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "web_application_config": {
    "starting_page_path": "/root",
    "url_blacklist": "/admin,contact,profile/settings",
    "auth_type": "unauthenticated",
    "login_form_username_field_name": "username",
    "login_form_password_field_name": "password"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "auth_type": "unauthenticated",
    "url_blacklist": "/admin,contact,profile/settings",
    "starting_page_path": "/root",
    "login_form_username_field_name": "username",
    "login_form_password_field_name": "password",
    "service_id": 1,
    "created_at": "2018-05-09T21:04:33.669Z",
    "updated_at": "2018-05-09T21:35:53.326Z"
  }
}
```

Updates an existing web application config.

#### HTTP Request

`PUT /v1/web_application_configs/{web_application_config.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
web_application_config[starting_page_path] | (Default is "/") If your service requires a specific starting page
web_application_config[url_blacklist] | (Default is none) If you would like to blacklist any URLs from test runs
web_application_config[auth_type] | (Default is "unauthenticated") An authentication method for your web service. (**unauthenticated** for no authentication, **digest** for HTTP Digest authentication, **form** for HTML form field authentication, or **basic** for HTTP Basic authentication)
web_application_config[login_form_username_field_name] | (Default is "username") Value for the name attribute for the "username" field of your service's HTML authentication form. (if you choose "form" for authentication method)
web_application_config[login_form_password_field_name] | (Default is "password") Value for the name attribute for the "password" field of your service's HTML authentication form. (if you choose "form" for authentication method)

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Web Application Service

```http
DELETE /v1/web_application_configs/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing web application config.

#### HTTP Request

`DELETE /v1/web_application_configs/{web_application_config.id}`

#### Body Parameters

None

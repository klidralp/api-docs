## Service Identities

Service Identities are authentication credentials for a machine's service.

### Create a Service Identity

```http
POST /v1/services/1/service_identities HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "service_identity": {
    "username": "john",
    "password": "John123!"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "username": "john",
    "password": "John123!",
    "service_id": 1,
    "created_at": "2018-05-10T01:05:21.079Z",
    "updated_at": "2018-05-10T01:05:21.079Z"
  }
}
```

#### HTTP Request

`POST /v1/services/{service.id}/service_identities`

#### Body Parameters

Parameter | Description
--------- | -----------
service_identity[username] | Username your service requires
service_identity[password] | Password your service requires

### Retrieve a specific Service Identity

```http
GET /v1/service_identities/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "username": "john",
    "password": "John123!",
    "service_id": 1,
    "created_at": "2018-05-10T01:05:21.079Z",
    "updated_at": "2018-05-10T01:05:21.079Z"
  }
}
```

Retrieves the details of the requested service identity.

#### HTTP Request

`GET /v1/service_identities/{service_identity.id}`

#### GET Parameters

None

### Update an existing Service Identity

```http
PUT /v1/service_identities/1 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "service_identity": {
    "username": "john",
    "password": "NewPassword123!"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "username": "john",
    "password": "NewPassword123!",
    "service_id": 1,
    "created_at": "2018-05-10T01:05:21.079Z",
    "updated_at": "2018-05-10T01:11:04.544Z"
  }
}
```

Updates an existing service identity.

#### HTTP Request

`PUT /v1/service_identities/{service_identity.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
service_identity[username] | Username your service requires
service_identity[password] | Password your service requires

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete a Service Identity

```http
DELETE /v1/service_identities/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing service identity.

#### HTTP Request

`DELETE /v1/service_identities/{service_identity.id}`

#### Body Parameters

None

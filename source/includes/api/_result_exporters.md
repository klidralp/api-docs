## Result Exporters

### Create a Result Export Job

```http
POST /v1/result_exporters HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "result_ids": [1, 2, 3, 4]
}
```

```http
HTTP/1.1 201 OK
Content-Type: application/json

{
  "message": "Queuing 4 resutls for export"
}
```

Creating a new instance of Result Exporter will trigger an export of the specified test results to
all configured Export Queues for the Organization. All Results must belong to the same Organization.

#### HTTP Request

POST /v1/result_exporters

#### Body Parameters

Parameter | Description
--------- | -----------
result_ids | An array of result IDs to export. Must all belong to the same Organization

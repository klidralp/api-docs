## Infosec Export Queues

### Create an Infosec Export Queue

```http
POST /v1/organizations/asig/infosec_export_queues HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "infosec_export_queue": {
    "infosec_export_queue_configuration_attributes": {
      "ctsm_id": "CTSM-1"
    },
    "auto_sync": true
  }
}
```

```http
HTTP/1.1 201 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "type": "infosec_export_queue",
    "created_by": "norad_tester",
    "created_at": "2017-04-06T23:49:00.000Z",
    "updated_at": "2017-04-06T23:49:00.000Z",
    "infosec_export_queue_configuration": {
      "id": 1,
      "ctsm_id": "CTSM-1"
    },
    "organization_id": 1,
    "auto_sync": true
  }
}
```

Creates a new Cisco Infosec export queue in the specified organization. Creating this queue will
allow exporting results directly to Infosec's Jira instance. If you'd like to automatically export
results when a scan is complete, simply set the auto_sync option to true.

Test results are synced to the issue associated with the PSB of the test. They appear in a separate
"validation" tab, and the issue itself has a "last Norad result" date field.

#### HTTP Request

`POST /v1/organiziations/{organization.slug}/infosec_export_queues`

#### Body Parameters

Parameter | Description
--------- | -----------
organization.slug | The organization slug of the organization to which you wish to add the Infosec export queue
infosec_export_queue[auto_sync] | Boolean to toggle whether or not results should be auto synced to Infosec Jira
infosec_export_queue[infosec_export_queue_configuration][ctsm_id] | CTSM ID. CTSM stands for "Cloud Tenant Security Management", it's an abbreviated project name in Jira. It accepts values of the form "CTSM-<number>".

### Retrieve a Specific Infosec Export Queue

```http
GET /v1/infosec_export_queues/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json
{
  "response": {
    "id": 1,
    "type": "infosec_export_queue",
    "created_by": "norad_tester",
    "created_at": "2017-04-06T23:49:00.000Z",
    "updated_at": "2017-04-06T23:49:00.000Z",
    "infosec_export_queue_configuration": {
      "id": 1,
      "ctsm_id": "CTSM-1"
    },
    "organization_id": 1,
    "auto_sync": true
  }
}
```

Retrieves the details of the requested Infosec export queue.

#### HTTP Request

`GET /v1/infosec_export_queues/{infosec_export_queue.id}`

#### GET Parameters

None

### Update a Infosec Export Queue

```http
PUT /v1/infosec_export_queues/1 HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "infosec_export_queue": {
    "infosec_export_queue_configuration_attributes": {
      "ctsm_id": "CTSM-2"
    },
    "auto_sync": false
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "type": "infosec_export_queue",
    "created_by": "norad_tester",
    "created_at": "2017-04-06T23:49:00.000Z",
    "updated_at": "2017-04-06T23:49:00.000Z",
    "organization_id": 1,
    "infosec_export_queue_configuration": {
      "id": 1,
      "ctsm_id": "CTSM-2"
    },
    "auto_sync": false
  }
}
```

Updates an existing Infosec export queue.

#### HTTP Request

`PUT /v1/infosec_export_queues/{infosec_export_queue.id}`

#### Body Parameters

Parameter | Description
--------- | -----------
infosec_export_queue[auto_sync] | Boolean to toggle whether or not results should be auto synced to Infosec Jira

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete an Infosec Export Queue

```http
DELETE /v1/infosec_export_queues/1 HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing Infosec export queue.

#### HTTP Request

`DELETE /v1/infosec_export_queues/{infosec_export_queue.id}`

#### Body Parameters

None

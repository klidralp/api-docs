## API Tokens

API tokens are used as the authentication method for nearly all end points in the Norad API.  Users are given one API token.  This token should be protected in the same way an account password would need to be protected.  In the event that a user's API token is compromised or unintentionally disclosed a user may recreate their token value.  Once an API token is recreated any previous values for that token will be instantly deactivated.

### Recreate an API Token

```http
PUT /v1/users/demouser/api_token HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "value": <new-api-token-value>,
    "state": "active"
  }
}
```

Recreates the API token for a user.

#### HTTP Request

`PUT /v1/users/{user.id}/api_token`

#### PUT Parameters

None

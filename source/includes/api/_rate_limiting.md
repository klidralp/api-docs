## Rate Limiting

> Sample rate limited request:

```http
HTTP/1.1 429 Too Many Requests
X-RateLimit-Limit: 7
X-RateLimit-Remaining: 0
X-RateLimit-Reset: 2017-10-05 14:30:01 UTC
Date: Thu, 05 Oct 2017 14:30:00 GMT
```

Rate limiting of the API is on a per-IP basis. Your requests will be throttled after 7 requests per second.

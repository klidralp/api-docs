## Scans

### Start a Scan for an Organization

```http
POST /v1/organizations/asig/scan HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "error_details": null,
    "containers": [
      "containers": ["norad-registry.cisco.com:5000/qualys:0.0.1", "norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1"]
    ],
    "commandable": {
      "id": 1,
      "uid": "ASIG",
      "slug": "asig",
      "token": "fa8068bad1dcc8b70aa926ae063695112f4091a58bb2838f97ce21938c3f4df6",
      "machine_count": 2,
      "configuration": {
        "id": 1,
        "auto_approve_docker_relays": false,
        "organization_id": 1,
        "use_relay_ssh_key": false
      }
    },
    "assessments": []
  }
}
```

This endpoint is an abstraction for Docker Commands. If you would like to start a scan that runs all
of your required and explicitly enabled containers, this is the endpoint to use. It results in a new
docker command for the specified organization. Spawns one or more Docker containers on the backend
to perform the scan. The containers will be started on a Relay if one is configured for
the organization.

#### HTTP Request

`POST /v1/organizations/{organization.slug}/scan`

#### Body Parameters

None

### Start a Scan for a Machine

```http
POST /v1/machines/1/scan HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 23,
    "error_details": null,
    "containers": [
      "containers": ["norad-registry.cisco.com:5000/qualys:0.0.1", "norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1"]
    ],
    "commandable": {
      "id": 10,
      "ip": "192.1681.100",
      "fqdn": "myserver.local",
      "description": "This is my best server",
      "latest_assessment_stats": {
        "passing": 0,
        "warning": 0,
        "failing": 0,
        "erroring": 0,
        "informing": 0
      },
      "organization_id": 1,
      "agent": null,
      "ssh_config": null
    },
    "assessments": []
  }
}
```

This endpoint is an abstraction for Docker Commands. If you would like to start a scan that runs all
of your required and explicitly enabled containers, this is the endpoint to use. It results in a new
docker command for the specified machine. Spawns one or more Docker containers on the backend to
perform the scan. The containers will be started on a Relay if one is configured for the
organization.

#### HTTP Request

`POST /v1/machines/{machine.id}/scan`

#### Body Parameters

None

## Result Export Queues

### List all Result Export Queues

```http
GET /v1/organizations/asig/result_export_queues HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "type": "infosec_export_queue",
      "created_by": "norad_tester",
      "created_at": "2017-04-06T23:49:00.000Z",
      "updated_at": "2017-04-06T23:49:00.000Z",
      "infosec_export_queue_configuration": {
        "id": 1,
        "ctsm_id": "CTSM-1"
      },
      "organization_id": 1,
      "auto_sync": true
    },
    {
      "id": 1,
      "type": "jira_export_queue",
      "created_by": "norad_tester",
      "created_at": "2017-04-06T23:49:00.000Z",
      "updated_at": "2017-04-06T23:49:00.000Z",
      "organization_id": 1,
      "auto_sync": false,
      "custom_jira_configuration": {
        "id": 1,
        "created_at": "2017-04-06T23:49:00.000Z",
        "updated_at": "2017-04-06T23:49:00.000Z",
        "title": "Test Result Export Queue",
        "site_url": "https://jira.example.invalid",
        "project_key": "TREQ",
        "username": "jira_admin_tester",
        "result_export_queue_id": 1
      }
    }
  ]
}
```

Returns an index listing all result export queues for an organization. These queues can be
configured to support auto export of results to an 3rd party tracker (such as Jira) when a scan is
completed as well as selective export of individual results. Currently, a user can export to an
arbitrary Jira instance or directly to Cisco Infosec's Jira instance.

#### HTTP Request

GET /v1/organizations/{organization.slug}/result_export_queues HTTP/1.1

#### GET Parameters

None

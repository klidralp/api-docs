## Provisions

This endpoint supports creation of Provisions. A Provision links a Requirement to a Security Container.

### Create a provision

Creates a new provision. 

```http
POST /v1/provisions HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "provision": {
    "name": "Sample Provision",
    "security_container_id": 1
   }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "security_container_id": 1,
    "requirement_id": 1
  }
}
```

#### HTTP Request

`POST /v1/provisions`

#### Body Parameters

Parameter | Description
--------- | -----------
provision[security_container_id] | ID of Security Container Provision will belong to

### Delete a provision

Deletes a provision.

```http
DELETE /v1/provisions/{id} HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 OK
Content-Type: text/html
```

#### HTTP Request

`DELETE /v1/provisions/{id}`

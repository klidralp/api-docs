## Organizations

### List all Organizations

Returns an index listing of all organizations in the system.

```http
GET /v1/organizations HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": [
    {
      "id": 1,
      "uid": "ASIG",
      "slug": "asig",
      "token": "DEADBEEF",
      "machine_count": 3,
      "machine_assessment_summary": {
        "erroring": 0,
        "failing": 0,
        "warning": 0,
        "passing": 2,
        "informing": 1,
        "pending": 0
      },
      "configuration": {
        "id": 1,
        "auto_approve_docker_relays": false,
        "use_relay_ssh_key": false,
        "organization_id": 1
      },
      "organization_errors": [
        {
          "type": "UnreachableMachineError",
          "message": "There are machines in your Organization in a private network, but no verified Relay is available to scan them.",
          "organization_id": 1,
          "errable_type": null,
          "errable_id": null
        },
        {
          "type": "KeyPairAssignmentWithoutSshServiceError",
          "message": "You have added an SSH Key Pair to machine Foobar but no SSH Service is registered for this machine.",
          "organization_id": 1,
          "errable_type": "Machine",
          "errable_id": 8
      ]
    },
    {
      "id": 2,
      "uid": "CIS Infosec",
      "slug": "cis-infosec",
      "token": null,
      "machine_count": 5,
      "machine_assessment_summary": {
        "erroring": 1,
        "failing": 0,
        "warning": 0,
        "passing": 3,
        "informing": 1,
        "pending": 0
      },
      "configuration": null,
      "organization_errors": []
    }
  ]
}
```

#### HTTP Request

`GET /v1/organizations`

#### GET Parameters

None

<aside class="notice">You will only see the token value if you have certain privileges within the
Organization</aside>

<aside class="notice">You will only see the configuration value if you have certain privileges
within the Organization</aside>

### Create an Organization

```http
POST /v1/organizations HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "organization": {
    "uid": "new org name"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "uid": "new org name",
    "slug": "new-org-name",
    "token": "DEADBEEF",
    "machine_count": 0,
    "machine_assessment_summary": {
      "erroring": 0,
      "failing": 0,
      "warning": 0,
      "passing": 0,
      "informing": 0,
      "pending": 0
    },
    "configuration": {
      "id": 3,
      "auto_approve_docker_relays": false,
      "use_relay_ssh_key": false,
      "organization_id": 3
    },
    "organization_errors": []
  }
}
```

Creates a new organization.

#### HTTP Request

`POST /v1/organiziations`

#### Body Parameters

Parameter | Description
--------- | -----------
organization[uid] | The name of the Organization being created

<aside class="notice">You will be granted administration privileges on the Organization you
create</aside>

### Retreive a specific Organization

```http
GET /v1/organizations/asig HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "uid": "ASIG",
    "slug": "asig",
    "token": "DEADBEEF",
    "machine_count": 3,
    "machine_assessment_summary": {
      "erroring": 0,
      "failing": 0,
      "warning": 0,
      "passing": 2,
      "informing": 0,
      "pending": 1
    },
    "configuration": {
      "id": 1,
      "auto_approve_docker_relays": false,
      "use_relay_ssh_key": false,
      "organization_id": 1
    }
    "organization_errors":
      {
        "type": "UnreachableMachineError",
        "message": "There are machines in your Organization in a private network, but no verified Relay is available to scan them.",
        "organization_id": 1
      }
    ]
  }
}
```

Retrieves the details of the requested organization.

#### HTTP Request

`GET /v1/organizations/{organization.slug}`

#### GET Parameters

None

<aside class="notice">You will only see the token value if you have certain privileges within the
Organization</aside>

<aside class="notice">You will only see the configuration value if you have certain privileges
within the Organization</aside>

### Update an Organization

```http
PUT /v1/organizations/asig HTTP/1.1
Content-Type: application/json
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token

{
  "organization": {
    "uid": "a new name"
  }
}
```

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "response": {
    "id": 1,
    "uid": "a new name",
    "slug": "a-new-name",
    "token": "DEADBEEF",
    "machine_count": 3,
    "machine_assessment_summary": {
      "erroring": 0,
      "failing": 0,
      "warning": 0,
      "passing": 2,
      "informing": 0,
      "pending": 1
    },
    "configuration": {
      "id": 1,
      "auto_approve_docker_relays": false,
      "use_relay_ssh_key": false,
      "organization_id": 1
    },
    "organization_errors": []
  }
}
```

Updates an existing organization.

#### HTTP Request

`PUT /v1/organizations/{organization.slug}`

#### Body Parameters

Parameter | Description
--------- | -----------
organization[uid] | The name you would like to set for the organization

<aside class="notice">This endpoint also supports the <code>PATCH</code> verb</aside>

### Delete an Organization

```http
DELETE /v1/organizations/asig HTTP/1.1
Host: norad.cisco.com
Authorization: Token token=my_norad_api_token
```

```http
HTTP/1.1 204 NO CONTENT
Content-Type: application/json
```

Removes an existing organization.

#### HTTP Request

`DELETE /v1/organizations/{organization.slug}`

#### Body Parameters

None

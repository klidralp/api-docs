# API Documentation

You can use the Norad API endpoints to assess the security posture of your servers.

<aside class="notice">
The API is available by accessing <b>https://norad.cisco.com:8443</b>.
</aside>

<aside class="notice">The current version of the API is <b>1</b>.</aside>

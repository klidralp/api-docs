# Norad API Docs

### Norad SLA

The Norad SLA can be found in the support repo:

 * [Norad Support](https://norad-gitlab.cisco.com/norad/support)
 * [Norad SLA](https://norad-gitlab.cisco.com/norad/support/blob/master/docs/Norad-SLA.docx)

### How to report a problem

#### Creating issues

If you notice a problem with Norad, please feel free to create an issue in our [support repo](https://norad-gitlab.cisco.com/norad/support).  Guidelines for creating an issue are in the [Reporting-Issues.md](https://norad-gitlab.cisco.com/norad/support/blob/master/Reporting-Issues.md) file located in the repo.

By filing an issue, it allows us to track and have a conversation related to the issue.  It also allows others to compare any issues they are having and whether or not there is already a solution to their problem.

In order to create an issue, you will need to sign in with Cisco CEC credentials.

#### Norad Security Mailing List

If you find a security vulnerability within the Norad platform, please practice responsible disclosure and send an encrypted email to the following address:

* [norad-security@cisco.com](mailto:norad-security@cisco.com)

Using this GPG Key:

* [Norad Security GPG Key on https://pgp.mit.edu](https://pgp.mit.edu/pks/lookup?op=vindex&search=0x82D3E259647DCC9E)
* [Directly download GPG Key from norad.cisco.com/docs](/resources/gpg_keys/norad-security.gpg.pub)

We will promptly answer any messages related to potential security problems with the platform.
